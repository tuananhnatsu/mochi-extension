import {
  callGgSuggestion,
  callTracking,
  callGetIP,
} from '../api/callApi/google';
import { xml2json, parseXml } from '../helpers/xml2json';
import { setGgSuggestion } from '../actions/index';
import _ from 'lodash';

export function getGgSuggestion(keyword) {
  return (dispatch) => {
    //dispatch()
    return callGgSuggestion(keyword).then(
      (res) => {
        let suggestionsLength = 0;
        const width = screen.width;
        if (width > 1400) {
          suggestionsLength = 8;
        } else {
          suggestionsLength = 4;
        }
        const suggestions = _.get(
          JSON.parse(xml2json(parseXml(res.data), '')),
          'toplevel.CompleteSuggestion',
          []
        );
        dispatch(setGgSuggestion(_.take(suggestions, suggestionsLength)));
      },
      (err) => {}
    );
  };
}

export async function trackingAction(payload) {
  const ipResponse = await callGetIP();
  const rowId = await chrome.storage.local.get('rowId');
  const email = await chrome.storage.local.get('email');
  const params = { ...payload, ip: ipResponse.data.ip };
  if (rowId.rowId) {
    params.rowId = rowId.rowId;
  }
  if (email.email) {
    params.email = email.email;
  }
  return callTracking(params).then((res) => {
    if (res.data.result == 'success') {
      chrome.storage.local.set({ rowId: res.data.rowId });
    }
  });
}
