import {
  callApiLogin,
  callApiGetProfile,
  callApiRegister,
  callFeedback,
  callApiGetStatistics,
  callApiLoginViaThirdParty,
} from '../api/callApi/account';
import _ from 'lodash';
import { ERROR_CODE } from '../constants/common';
import {
  setErrorLogin,
  setProfile,
  setErrorRegister,
  setStatistics,
} from '../actions';
import $ from 'jquery';
import { promiseTabCurrent } from '../helpers/chrome';
import * as firebase from 'firebase/app';
import * as firebaseAuth from 'firebase/auth';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyA7XZFrn_ue3sR8RNfXZdn74F0dle0GMpI',
  authDomain: 'mochien-7880c.firebaseapp.com',
  projectId: 'mochien-7880c',
  storageBucket: 'mochien-7880c.appspot.com',
  messagingSenderId: '158797874498',
  appId: '1:158797874498:web:a029802e1da3f7edfdb4db',
  measurementId: 'G-90NXGYPE7C',
});

const auth = firebaseAuth.initializeAuth(firebaseApp, {
  persistence: firebaseAuth.browserSessionPersistence,
  popupRedirectResolver: firebaseAuth.browserPopupRedirectResolver,
});

export function login(payload, openerTabId) {
  return (dispatch) => {
    //dispatch()
    return callApiLogin(payload).then(
      async (res) => {
        const data = _.get(res, 'data', {});
        if (data.code == 1) {
          //Login succesfully
          $('#login-succeed-modal').css('display', 'block');
          await chrome.storage.local.set({ identity: data.user });
          const currentTab = await promiseTabCurrent();
          setTimeout(() => {
            if (openerTabId)
              chrome.tabs.update(Number(openerTabId), { highlighted: true });
            chrome.tabs.remove(Number(currentTab.id));
          }, 1000);
        } else {
          // Login fail
          if (data.lang == ERROR_CODE.EMAIL_NOT_EXIST) {
            dispatch(setErrorLogin({ email: `*${data.msg}` }));
          }
          if (data.lang == ERROR_CODE.PASSWORD_INCORRECT) {
            dispatch(setErrorLogin({ password: `*${data.msg}` }));
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

export async function logout(openerTabId) {
  await chrome.storage.local.remove('identity');
  firebaseAuth.signOut(auth);
  // const currentTab = await promiseTabCurrent();
  // if (openerTabId)
  // await chrome.tabs.update(Number(openerTabId), { highlighted: true });
  await chrome.tabs.update({
    url: 'https://mochidemy.com/extension-page-login/login',
  });
  // await chrome.tabs.remove(Number(currentTab.id));
}

export function register(payload, openerTabId) {
  return (dispatch) => {
    //dispatch()
    return callApiRegister({ ...payload, trial_course: 1 }).then(
      async (res) => {
        const data = _.get(res, 'data', {});
        if (data.code == 1) {
          //Register succesfully
          $('#login-succeed-modal').css('display', 'block');
          // await chrome.storage.local.set({ identity: data.user });
          await login(payload, openerTabId)(dispatch);
          const currentTab = await promiseTabCurrent();
          setTimeout(async () => {
            if (openerTabId)
              await chrome.tabs.update(Number(openerTabId), {
                highlighted: true,
              });
            // chrome.tabs.create({
            //   url: 'https://mochidemy.com/extension-sign-up-success',
            // });
            await chrome.tabs.remove(Number(currentTab.id));
          }, 1000);
        } else {
          // Login fail
          if (data.errors && data.errors.email && data.errors.email.length) {
            dispatch(setErrorRegister({ email: `*${data.errors.email[0]}` }));
          }
          if (
            data.errors &&
            data.errors.password &&
            data.errors.password.length
          ) {
            dispatch(
              setErrorRegister({ password: `*${data.errors.password[0]}` })
            );
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

export function getProfile(params) {
  return (dispatch) => {
    return callApiGetProfile(params).then(
      (res) => {
        const data = _.get(res, 'data', {});
        if (data.code == 1) {
          dispatch(setProfile(data.data));
          chrome.storage.local.set({ email: data.data.email });
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

export function getStatistics(params) {
  return (dispatch) => {
    return callApiGetStatistics(params).then(
      (res) => {
        const data = _.get(res, 'data', {});
        if (data.code == 1) {
          const statistic = _.get(data, 'data.statistic', []);
          const amountWordsHaveToReview = _.sumBy(statistic, (o) => o.count);
          const _statOfGreaterThan5 = _.reduce(
            statistic,
            (sum, n) => {
              if (n.proficiency >= 6) {
                sum.count = sum.count + n.count;
                return sum;
              }
              return sum;
            },
            { proficiency: 5, count: 0 }
          );
          const _statOfLevel4 = _.reduce(
            statistic,
            (sum, n) => {
              if (n.proficiency == 4 || n.proficiency == 5) {
                sum.count = sum.count + n.count;
                return sum;
              }
              return sum;
            },
            { proficiency: 4, count: 0 }
          );
          const _statOfLessThan4 = _.reduce(
            statistic,
            (result, el) => {
              if (el.proficiency < 4 && el.proficiency > 0) {
                result.push(el);
                return result;
              }
              return result;
            },
            []
          );
          const stat = [
            ..._statOfLessThan4,
            _statOfLevel4,
            _statOfGreaterThan5,
          ];

          const max_stat = _.maxBy(stat, 'count');

          stat.forEach((item) => {
            item.height = (item.count / max_stat.count) * 100 + '%';
          });

          for (let i = 0; i < 5; i++) {
            if (_.find(stat, ['proficiency', i + 1])) continue;
            stat.push({
              height: '0',
              count: '0',
              proficiency: i + 1,
            });
          }
          dispatch(
            setStatistics({
              ...data.data,
              statistic: stat,
              amountWordsHaveToReview,
            })
          );
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

export function feedback(payload) {
  return (dispatch) => {
    return callFeedback(payload).then(
      (res) => {
        const data = _.get(res, 'data', {});
        if (data.result == 'success') {
          $('#thanks-modal').css({ display: 'block' });
          setTimeout(() => {
            $('#thanks-modal').css({ display: 'none' });
          }, 3000);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

function loginViaThirdParty(payload, openerTabId) {
  return callApiLoginViaThirdParty(payload).then(
    async (res) => {
      const data = _.get(res, 'data', {});
      if (data.code == 1) {
        //Login succesfully
        $('#login-succeed-modal').css('display', 'block');
        await chrome.storage.local.set({ identity: data.user });
        const currentTab = await promiseTabCurrent();
        setTimeout(() => {
          if (openerTabId)
            chrome.tabs.update(Number(openerTabId), { highlighted: true });
          chrome.tabs.remove(Number(currentTab.id));
        }, 1000);
      }
    },
    (err) => {
      console.log(err);
    }
  );
}

export async function loginWithFacebook(openerTabId) {
  const provider = new firebaseAuth.FacebookAuthProvider();
  const result = await firebaseAuth.signInWithPopup(auth, provider);
  const payload = {
    email: result.user.email,
    name: result.user.displayName,
    phone: result.user.phoneNumber,
    provider: 'facebook',
    provider_id: result.providerId,
    lang: 'vn',
  };
  await loginViaThirdParty(payload, openerTabId);
}

export async function loginWithGoogle(openerTabId) {
  const provider = new firebaseAuth.GoogleAuthProvider();
  const result = await firebaseAuth.signInWithPopup(auth, provider);
  const payload = {
    email: result.user.email,
    name: result.user.displayName,
    phone: result.user.phoneNumber,
    provider: 'google',
    provider_id: result.providerId,
    lang: 'vn',
  };
  await loginViaThirdParty(payload, openerTabId);
}

export async function loginWithApple(openerTabId) {
  const provider = new firebaseAuth.OAuthProvider('apple.com');
  provider.addScope('email');
  provider.addScope('name');
  provider.setCustomParameters({
    // Localize the Apple authentication screen in Vietnam.
    locale: 'vi',
  });
  const result = await firebaseAuth.signInWithPopup(auth, provider);
  const payload = {
    email: result.user.email,
    name: result.user.displayName,
    phone: result.user.phoneNumber,
    provider: 'apple',
    provider_id: result.providerId,
    lang: 'vn',
  };
  await loginViaThirdParty(payload, openerTabId);
}
