import { callSearch, callSaveWord } from '../api/callApi/dictionary';
import _ from 'lodash';
import { setResultSearch } from '../actions';
import $ from 'jquery';
import { getStatistics } from './account.invoker';
import { promiseGetStorage } from '../helpers/chrome';

export function search(payload) {
  return async (dispatch) => {
    const identity = await promiseGetStorage('identity');
    const user_token = _.get(identity, 'identity.user_token', '');
    if (user_token) {
      payload = { ...payload, user_token };
    }
    return callSearch(payload).then(
      (res) => {
        // console.log(res);
        const data = _.get(res, 'data', {});
        if (data.code == 1) {
          dispatch(setResultSearch(data.data));
        } else {
          $('#search-error-modal').css({ display: 'block' });
        }
      },
      (err) => {
        console.log(err);
      }
    );
  };
}

export function saveWord(payload, statistic = {}, profile = {}) {
  return (dispatch) => {
    if (profile.expired_day && new Date(profile.expired_day) >= new Date()) {
      // ACCOUNT TYPE: PAID USER
      if (
        statistic.review_time &&
        new Date(statistic.review_time) < new Date()
      ) {
        // Tới giờ ôn tập

        if (statistic.review_count >= 100) {
          $('#saving-more-than-modal').css({ display: 'block' });
          return;
        }
      }
      return callSaveWord(payload).then(
        async (res) => {
          const data = _.get(res, 'data', {});
          if (data.code == 1) {
            await getStatistics({ user_token: profile.user_token })(dispatch);
            // TODO: Check user'statistic to show approciate popup
            if (
              statistic.review_time &&
              new Date(statistic.review_time) < new Date()
            ) {
              // Tới giờ ôn tập
              $('#saving-ontime-to-train-modal').css({ display: 'block' });
              return;
            } else {
              // Chưa Tới giờ ôn tập
              if (
                statistic.total == 0 ||
                !_.some(
                  statistic.statistic,
                  (item) => item.count > 0 && item.proficiency > 0
                )
              ) {
                // Chưa sử dụng website
                $('#saving-non-used-website-modal').css({ display: 'block' });
                return;
              } else {
                // Đã sử dụng website
                $('#saving-not-in-time-modal').css({ display: 'block' });
                return;
              }
            }
          } else {
            if (Number(data.code) == 2) {
              switch (data.case) {
                case 1: {
                  $('#saving-more-than-level-1-modal').css({
                    display: 'block',
                  });
                  break;
                }
                case 2: {
                  $('#saving-succeed-modal').css({ display: 'block' });
                  break;
                }
                case 3: {
                  const amountOfProficient1 = _.get(
                    statistic,
                    'statistic[0].count',
                    0
                  );
                  if (amountOfProficient1 >= 100) {
                    $('#saving-more-than-level-1-modal').css({
                      display: 'block',
                    });
                  }
                  break;
                }
                default:
                  break;
              }
              return;
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      // ACCOUNT TYPE: FREE USER
      // const amountOfProficient1 = _.get(statistic, 'statistic[0].count', 0);
      // if (amountOfProficient1 >= 50) {
      //   $('#saving-more-than-level-1-modal').css({ display: 'block' });
      //   return;
      // }
      // if (statistic && statistic.total >= 30) {
      //   $('#saving-succeed-modal').css({ display: 'block' });
      //   return;
      // } else {
      return callSaveWord(payload).then(
        async (res) => {
          const data = _.get(res, 'data', {});
          if (data.code == 1) {
            await getStatistics({ user_token: profile.user_token })(dispatch);
            if (
              statistic.review_time &&
              new Date(statistic.review_time) >= new Date()
            ) {
              // Chưa tới giờ ôn tập
              if (
                statistic.total == 0 ||
                !_.some(
                  statistic.statistic,
                  (item) => item.count > 0 && item.proficiency > 0
                )
              ) {
                // Chưa dùng website
                $('#saving-non-used-website-modal').css({ display: 'block' });
                return;
              } else {
                // Đã dùng website
                $('#saving-not-in-time-modal').css({ display: 'block' });
                return;
              }
            } else {
              // Đến giờ ôn tập
              $('#saving-ontime-to-train-modal').css({ display: 'block' });
              return;
            }
          } else {
            if (Number(data.code) == 2) {
              switch (data.case) {
                case 1: {
                  $('#saving-more-than-level-1-modal').css({
                    display: 'block',
                  });
                  break;
                }
                case 2: {
                  $('#saving-succeed-modal').css({ display: 'block' });
                  break;
                }
                default:
                  break;
              }
              return;
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
      // }
    }
  };
}

export function saveWordForOtherPages(payload, statistic = {}, profile = {}) {
  return (dispatch) => {
    const module = document.getElementById('mochi-extension-root');
    const shadowRoot = module.shadowRoot;

    if (profile.expired_day && new Date(profile.expired_day) >= new Date()) {
      //Paid ACC
      if (
        statistic.review_time &&
        new Date(statistic.review_time) < new Date()
      ) {
        if (statistic.review_count >= 100) {
          shadowRoot.getElementById('saving-more-than-modal').style.display =
            'block';
          return;
        }
      }
      return callSaveWord(payload).then(
        async (res) => {
          const data = _.get(res, 'data', {});
          if (data.code == 1) {
            await getStatistics({ user_token: profile.user_token })(dispatch);
            // TODO: Check user'statistic to show approciate popup
            if (
              statistic.review_time &&
              new Date(statistic.review_time) < new Date()
            ) {
              // Tới giờ ôn tập
              shadowRoot.getElementById(
                'saving-ontime-to-train-modal'
              ).style.display = 'block';
              return;
            } else {
              if (
                statistic.total == 0 ||
                !_.some(
                  statistic.statistic,
                  (item) => item.count > 0 && item.proficiency > 0
                )
              ) {
                shadowRoot.getElementById(
                  'saving-non-used-website-modal'
                ).style.display = 'block';
                return;
              } else {
                shadowRoot.getElementById(
                  'saving-not-in-time-modal'
                ).style.display = 'block';
                return;
              }
            }
          } else {
            if (Number(data.code) == 2) {
              switch (data.case) {
                case 1: {
                  shadowRoot.getElementById(
                    'saving-more-than-level-1-modal'
                  ).style.display = 'block';
                  break;
                }
                case 2: {
                  shadowRoot.getElementById(
                    'saving-succeed-modal'
                  ).style.display = 'block';
                  break;
                }
                case 3: {
                  const amountOfProficient1 = _.get(
                    statistic,
                    'statistic[0].count',
                    0
                  );
                  if (amountOfProficient1 >= 100) {
                    shadowRoot.getElementById(
                      'saving-more-than-level-1-modal'
                    ).style.display = 'block';
                  }
                  break;
                }
                default:
                  break;
              }
              return;
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      // if (statistic && statistic.total >= 30) {
      //   shadowRoot.getElementById('saving-succeed-modal').style.display =
      //     'block';
      //   return;
      // } else {
      return callSaveWord(payload).then(
        async (res) => {
          const data = _.get(res, 'data', {});
          if (data.code == 1) {
            await getStatistics({ user_token: profile.user_token })(dispatch);
            if (
              statistic.review_time &&
              new Date(statistic.review_time) >= new Date()
            ) {
              // Chưa tới giờ ôn tập
              if (
                statistic.total == 0 ||
                !_.some(
                  statistic.statistic,
                  (item) => item.count > 0 && item.proficiency > 0
                )
              ) {
                // Chưa dùng website
                shadowRoot.getElementById(
                  'saving-non-used-website-modal'
                ).style.display = 'block';
                return;
              } else {
                // Đã dùng website
                shadowRoot.getElementById(
                  'saving-not-in-time-modal'
                ).style.display = 'block';
                return;
              }
            } else {
              // Đến giờ ôn tập
              shadowRoot.getElementById(
                'saving-ontime-to-train-modal'
              ).style.display = 'block';
              return;
            }
          } else {
            if (Number(data.code) == 2) {
              switch (data.case) {
                case 1: {
                  shadowRoot.getElementById(
                    'saving-more-than-level-1-modal'
                  ).style.display = 'block';
                  break;
                }
                case 2: {
                  shadowRoot.getElementById(
                    'saving-succeed-modal'
                  ).style.display = 'block';
                  break;
                }
                default:
                  break;
              }
              return;
            }
          }
        },
        (err) => {
          console.log(err);
        }
      );
      // }
    }
  };
}
