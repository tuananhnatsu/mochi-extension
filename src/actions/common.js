export const SET_BACKGROUND_IMAGE = "SET_BACKGROUND_IMAGE";
export const SET_QUOTES = "SET_QUOTES"

export const setBgImage = (data) => ({
    type: SET_BACKGROUND_IMAGE, data
})

export const setQuotes = (data) => ({
    type: SET_QUOTES, data
})