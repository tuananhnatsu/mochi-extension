export const SET_GG_SUGGESTION = "SET_GG_SUGGESTION"

export const setGgSuggestion = (data) => ({
    type: SET_GG_SUGGESTION, data
})