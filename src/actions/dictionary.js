export const SET_RESULT_SEARCH = "SET_RESULT_SEARCH"

export const setResultSearch = (data) => ({
    type: SET_RESULT_SEARCH, data
})