export const SET_ERROR_LOGIN = "SET_ERROR_LOGIN"
export const SET_ERROR_REGISTER = "SET_ERROR_REGISTER"
export const SET_PROFILE = "SET_PROFILE"
export const SET_STATISTICS = "SET_STATISTICS"

export const setErrorLogin = (data) => ({
    type: SET_ERROR_LOGIN, data
})
export const setErrorRegister = (data) => ({
    type: SET_ERROR_REGISTER, data
})
export const setProfile = (data) => ({
    type: SET_PROFILE, data
})
export const setStatistics = (data) => ({
    type: SET_STATISTICS, data
})