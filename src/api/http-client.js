import axios from 'axios';
import { PRIVATE_KEY, BASE_URL } from '../constants/common';
import _ from 'lodash';
import { promiseGetStorage } from '../helpers/chrome';

const HTTP = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Access-Control-Allow-Origin': '*',
    // "Current-Page-Url": window.location.href,
    PrivateKey: PRIVATE_KEY,
    DeviceType: 3,
    AppVersion: '2.0',
  },
});

HTTP.interceptors.request.use(
  async (config) => {
    const identityStore = await promiseGetStorage(['identity']);
    const user_token = _.get(identityStore, 'identity.user_token', '');
    config.data = {
      ...config.data,
      user_token,
    };
    return config;
  },
  (err) => Promise.reject(err)
);

export default HTTP;
