const Endpoints = {
  GOOGLE_API_SEARCH_QUERIES:
    'https://suggestqueries.google.com/complete/search',
  TRACKING_ACTION:
    'https://script.google.com/macros/s/AKfycbzxnRCbvlIzZJora5AymbRUIiO1wVBmXQ2xmGkSXVxmVpbrvTTDNyFWgFTtuDDhzTo_/exec',
  GET_CLIENT_IP: 'https://ipinfo.io',
  API_LOGIN: 'v3.0/signin-email',
  API_REGISTER: 'v3.1/register-ios',
  API_GET_PROFILE: 'v3.0/profile',
  API_GET_LEARNING_PROCESSING: 'v3.1/words/summary-ios',
  API_SEARCH: 'v3.1/words/dictionary-english',
  API_SAVE_WORD: 'v3.1/words/dictionary-english/add-word',
  API_FEEDBACK:
    'https://script.google.com/macros/s/AKfycbwXTpHDuxztM47mOiVKGN36pUg-k7fYeKudCaBHW-fM5FoxSf9uT5QMBN_GDsdkvMaG/exec',
  API_GET_STATISTICS: 'v3.1/words/summary-ios',
  API_LOGIN_VIA_THIRD_PARTY: 'v3.1/signin-with-fb-google',
};

export default Endpoints;
