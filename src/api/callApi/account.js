import HTTP from '../http-client';
import ENDPOINT from '../endpoints';
import axios from 'axios';

export function callApiLogin(payload) {
  return HTTP({
    url: ENDPOINT.API_LOGIN,
    method: 'POST',
    data: payload,
  });
}

export function callApiLoginViaThirdParty(payload) {
  return HTTP({
    url: ENDPOINT.API_LOGIN_VIA_THIRD_PARTY,
    method: 'POST',
    data: payload,
  });
}

export function callApiRegister(payload) {
  return HTTP({
    url: ENDPOINT.API_REGISTER,
    method: 'POST',
    data: payload,
  });
}

export function callApiGetProfile(payload) {
  return HTTP({
    url: ENDPOINT.API_GET_PROFILE,
    method: 'GET',
    params: payload,
  });
}

export function callApiGetStatistics(payload) {
  return HTTP({
    url: ENDPOINT.API_GET_STATISTICS,
    method: 'GET',
    params: payload,
  });
}

export function callFeedback(payload) {
  return axios.request({
    url: ENDPOINT.API_FEEDBACK,
    method: 'GET',
    params: payload,
  });
}
