import HTTP from '../http-client';
import ENDPOINT from '../endpoints';

export function callSearch(payload) {
    return HTTP({
        url: ENDPOINT.API_SEARCH,
        method: "GET",
        params: payload,
    });
}

export function callSaveWord(payload) {
    return HTTP({
        url: ENDPOINT.API_SAVE_WORD,
        method: "POST",
        data: payload,
    });
}