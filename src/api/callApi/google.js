import axios from 'axios';
import ENDPOINT from '../endpoints';

export function callGgSuggestion(keyword) {
  const url = `${ENDPOINT.GOOGLE_API_SEARCH_QUERIES}?output=toolbar&hl=vi&q=${keyword}`;
  return axios.get(url, {
    'Content-Type': 'application/xml; charset=utf-8',
  });
}

const convertToQuery = (payload) => {
  const result = Object.keys(payload).reduce((pre, cur, index) => {
    pre += `${cur}=${payload[cur]}`;
    if (index < Object.keys(payload).length - 1) {
      pre += '&';
    }
    return pre;
  }, '');
  return result;
};

export function callTracking(payload) {
  const url = `${ENDPOINT.TRACKING_ACTION}?${convertToQuery(payload)}`;
  return axios.get(url);
}

export function callGetIP() {
  return axios.get(ENDPOINT.GET_CLIENT_IP);
}
