import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import reducers from "../reducers";

const initilizeState = {}

function configureStore(state = initilizeState) {
    return createStore(reducers, state, applyMiddleware(thunk));
}

export default configureStore;