import React, { useState } from 'react';
import './Register.scss';
import mochi_register_icon from '../../assets/img/mochi-register-icon.png';
import left_arrow from '../../assets/img/left-arrow.png';
import { register } from '../../invoker/account.invoker';
import { useDispatch, useSelector } from 'react-redux';
import { setErrorRegister } from '../../actions';
import { Fragment } from 'react';
import { promiseTabCurrent } from '../../helpers/chrome';
import {
  loginWithFacebook,
  loginWithGoogle,
} from '../../invoker/account.invoker';
import LoginSucceed from '../../components/modals/LoginSucceed';

const Register = () => {
  const [account, setAccount] = useState({
    email: '',
    password: '',
    display_name: '',
  });

  const loginWithFb = async (e) => {
    e.preventDefault();
    await loginWithFacebook(openerTabId);
  };
  const loginWithGg = async (e) => {
    e.preventDefault();
    await loginWithGoogle();
  };

  const [openerTabId, setOpenerTabId] = useState(0);
  const [tab, setTab] = useState(0);

  useState(async () => {
    const cur = await promiseTabCurrent();
    setOpenerTabId(cur.openerTabId);
  }, []);
  const handleChange = (e) => {
    setAccount({
      ...account,
      [e.target.name]: e.target.value,
    });
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
      } else {
        dispatch(setErrorRegister({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
      } else {
        dispatch(setErrorRegister({ ...error, password: '' }));
      }
    }
  };

  const error = useSelector((state) => state.account.registerError);

  const handleBlur = (e) => {
    if (e.target.name == 'display_name') {
      if (e.target.value.length < 3) {
        dispatch(
          setErrorRegister({
            ...error,
            display_name: '*Tên hiển thị cần có từ 3-15 ký tự',
          })
        );
      } else {
        dispatch(setErrorRegister({ ...error, display_name: '' }));
      }
    }
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
        dispatch(
          setErrorRegister({
            ...error,
            email: '*Email sai định dạng, bạn kiểm tra lại nhé',
          })
        );
      } else {
        dispatch(setErrorRegister({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
        dispatch(
          setErrorRegister({
            ...error,
            password: '*Mật khẩu cần có ít nhất 06 ký tự',
          })
        );
      } else {
        dispatch(setErrorRegister({ ...error, password: '' }));
      }
    }
  };

  const dispatch = useDispatch();
  const onRegister = async (e) => {
    e.preventDefault();
    if (validateEmail(account.email) && account.password.length >= 6) {
      await register({ ...account, lang: 'vn' }, openerTabId)(dispatch);
    }
  };
  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };
  const closeTab = async () => {
    const currentTab = await promiseTabCurrent();
    chrome.tabs.remove(Number(currentTab.id));
  };
  return (
    <div className="container">
      <div className="mochi-login">
        <div className="mochi-login-title">
          {tab != 0 ? (
            <button className="btn btn-back" onClick={() => setTab(0)}>
              <img src={left_arrow} alt="left_arrow" width="15" height="15" />
            </button>
          ) : (
            <button className="btn btn-cancel" onClick={closeTab}>
              &#10006;
            </button>
          )}
          Tạo tài khoản mới
        </div>
        <div className="mochi-login-body">
          <h4>
            Bạn muốn tạo tài khoản <br /> bằng cách nào nhỉ?
          </h4>
          <img
            width="150"
            style={{ marginBottom: '2rem' }}
            className="mochi-register-icon"
            src={mochi_register_icon}
            alt="mochi_register_icon"
          />
          {tab == 0 ? (
            <Fragment>
              <div className="btn-section">
                <a
                  type="button"
                  className="btn btn-login-via-gg"
                  onClick={loginWithGg}
                >
                  Tạo tài khoản bằng G+
                </a>
                <a
                  type="button"
                  className="btn btn-login-via-fb"
                  onClick={loginWithFb}
                >
                  Tạo tài khoản bằng Facebook
                </a>
                {/* <a type="button" className="btn btn-login-via-apple">
              Đăng nhập với Apple
            </a> */}
              </div>
              <h5>HOẶC</h5>
              <div className="btn-section">
                <a
                  type="button"
                  className="btn btn-login-via-email"
                  onClick={() => setTab(1)}
                >
                  Tự tạo tài khoản với email
                </a>
              </div>
              <div style={{ fontWeight: 600, marginTop: '1rem' }}>
                Bạn đã có tài khoản?{' '}
                <a href="https://mochidemy.com/extension-page-login/login">
                  Đăng nhập ngay
                </a>
              </div>
            </Fragment>
          ) : (
            <form>
              <div className="form-section">
                <input
                  value={account.display_name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="text"
                  name="display_name"
                  className={
                    error.display_name
                      ? 'input-mochi-display_name invalid'
                      : 'input-mochi-display_name'
                  }
                  placeholder="Tên của bạn"
                />
                {error.display_name && (
                  <div className="error">{error.display_name}</div>
                )}
                <input
                  value={account.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="text"
                  name="email"
                  className={
                    error.email
                      ? 'input-mochi-mail invalid'
                      : 'input-mochi-mail'
                  }
                  placeholder="Nhập chính xác email của bạn"
                />
                {error.email && <div className="error">{error.email}</div>}
                <input
                  value={account.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="password"
                  name="password"
                  className={
                    error.password
                      ? 'input-mochi-mail invalid'
                      : 'input-mochi-mail'
                  }
                  placeholder="Tạo mật khẩu (dễ nhớ chút nhé ^^)"
                />
                {error.password && (
                  <div className="error">{error.password}</div>
                )}
              </div>
              <div className="btn-section">
                <input
                  type="submit"
                  onClick={onRegister}
                  className={
                    validateEmail(account.email) &&
                    account.password.length >= 6 &&
                    account.display_name.length >= 3
                      ? 'btn btn-login active'
                      : 'btn btn-login'
                  }
                  value="Tạo tài khoản"
                />
              </div>
            </form>
          )}
        </div>
      </div>
      <div className="mochi-modals">
        <LoginSucceed mode="register" />
      </div>
    </div>
  );
};
export default Register;
