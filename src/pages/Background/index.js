// Set active new tab
chrome.storage.local.set({ disableNewtab: false });

// Open new tab pointed to ExtensionWelcome when installed and ExtensionFarewell when uninstalled
chrome.runtime.onInstalled.addListener((details) => {
  if (details.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.tabs.create({
      url: 'https://mochidemy.com/extension-welcome',
    });
    chrome.runtime.setUninstallURL(
      'https://mochidemy.com/extension-see-you-later/'
    );
  }
});

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message === 'Authentication') {
    chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/login',
    });
  }
  if (message === 'Profile') {
    chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/',
    });
  }
  if (message === 'Register') {
    chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/register',
    });
  }
  sendResponse(message);
});

chrome.cookies.onChanged.addListener(async (changeInfo) => {
  if (changeInfo.cookie.domain == '.mochidemy.com') {
    if (changeInfo.cookie.name == 'mochiToken') {
      if (changeInfo.removed) {
        chrome.storage.local.remove(['identity', 'email', 'rowId']);
      } else {
        chrome.storage.local.set({
          identity: { user_token: changeInfo.cookie.value },
        });
      }
    } else if (changeInfo.cookie.name == 'disableNewTab') {
      if (changeInfo.cookie.value && Number(changeInfo.cookie.value) == 1) {
        chrome.storage.local.set({ disableNewtab: true });
      } else {
        chrome.storage.local.set({ disableNewtab: false });
      }
    } else if (changeInfo.cookie.name == 'defaultDict') {
      if (changeInfo.cookie.value == 'en-en') {
        chrome.storage.local.set({ modeDictionary: 'us' });
      } else {
        chrome.storage.local.set({ modeDictionary: 'vi' });
      }
    }
  }
});
