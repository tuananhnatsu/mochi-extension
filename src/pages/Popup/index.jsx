import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from '../../store';
import Popup from './Popup';
import './index.css';
const store = configureStore();
render(
  <Provider store={store}>
    <Popup />
  </Provider>,
  window.document.querySelector('#mochi-container')
);

if (module.hot) module.hot.accept();
