import React from 'react';
import './Popup.css';
import { initilizeModal } from '../Content/modules/modal';
import search_icon from '../../assets/img/search.png';
import dictionary_logo from '../../assets/img/dictionary-logo.png';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { search, saveWord } from '../../invoker/dictionary.invoker';
import { getProfile, getStatistics } from '../../invoker/account.invoker';
import { trackingAction } from '../../invoker/google.invoker';
import audio_icon_1 from '../../assets/img/audio-icon-1.png';
import audio_icon_blue_0 from '../../assets/img/audio-icon-blue-0.png';
import audio_icon_blue_1 from '../../assets/img/audio-icon-blue-1.png';
import audio_icon_blue_2 from '../../assets/img/audio-icon-blue-2.png';

import audio_icon_2 from '../../assets/img/audio-icon-2.png';
import audio_icon_red_0 from '../../assets/img/audio-icon-red-0.png';
import audio_icon_red_1 from '../../assets/img/audio-icon-red-1.png';
import audio_icon_red_2 from '../../assets/img/audio-icon-red-2.png';

import mochi_default_icon from '../../assets/img/mochi-default-icon.png';
import mochi_detective_icon from '../../assets/img/mochi-detective-icon.png';
import check_save_icon from '../../assets/img/check-save-icon.png';
import { setResultSearch } from '../../actions';
import InviteLogin from '../../components/modals/InviteLogin';
import SavingWordSucced from '../../components/modals/SavingWordSucced';
import SavingMoreThanOneHundredLevel1 from '../../components/modals/SavingMoreThanOneHundredLevel1';
import SavingMoreThanOneHundredWords from '../../components/modals/SavingMoreThanOneHundredWords';
import SavingSucceedWithPaidUser from '../../components/modals/SavingSucceedWithPaidUser';
import SavingWordSucceedWithPaidUserOnTrainingTime from '../../components/modals/SavingWordSucceedWithPaidUserOnTrainingTime';
import SavingSucceedWithoutNonUsedWebsite from '../../components/modals/SavingSucceedWithoutNonUsedWebsite';
import SearchError from '../../components/modals/SearchError';
import _, { reject } from 'lodash';
import $ from 'jquery';
import { promiseGetStorage, promiseTabQuery } from '../../helpers/chrome';

const VIEWS = {
  collapse: 'collapse',
  expand: 'expand',
};

const MODES = {
  vi: 'Anh - Việt',
  us: 'Anh - Anh',
};

const Popup = () => {
  const [key, setKey] = useState('');
  const [mode, setMode] = useState('vi');
  const [view, setView] = useState(VIEWS.collapse);
  const [identity, setIdentity] = useState({});
  const [iconBlue, setIconBlue] = useState(audio_icon_1);
  const [iconRed, setIconRed] = useState(audio_icon_2);

  const handleChange = (e) => {
    setKey(e.target.value);
    dispatch(setResultSearch({}));
  };

  const changeView = (e) => {
    e.preventDefault();
    setView(view == VIEWS.expand ? VIEWS.collapse : VIEWS.expand);
  };

  const handleClickSelect = (e) => {
    e.stopPropagation();
    closeAllSelect(e.target);
    e.target.nextSibling.classList.toggle('select-hide');
    e.target.classList.toggle('select-arrow-active');
  };
  const dispatch = useDispatch();
  const onSearch = async () => {
    await search({ key })(dispatch);
  };

  const onSearchSuggestion = async (e, key) => {
    e.preventDefault();
    setKey(key);
    await search({ key })(dispatch);
  };

  const playSound = async (link, type = 'blue') => {
    if (!link) return;
    const sound = new Audio(link);
    const sequentiallySetIcon = (icon) =>
      new Promise((resolve, reject) => {
        setTimeout(() => {
          type == 'blue' ? setIconBlue(icon) : setIconRed(icon);
          resolve(1);
        }, 200);
      });
    const animateSoundIcon = async () => {
      if (type == 'blue') {
        setIconBlue(audio_icon_blue_0);
        await sequentiallySetIcon(audio_icon_blue_1);
        await sequentiallySetIcon(audio_icon_blue_2);
        await sequentiallySetIcon(audio_icon_1);
      } else {
        setIconRed(audio_icon_red_0);
        await sequentiallySetIcon(audio_icon_red_1);
        await sequentiallySetIcon(audio_icon_red_2);
        await sequentiallySetIcon(audio_icon_2);
      }
    };
    await Promise.all([animateSoundIcon(), sound.play()]);
  };
  const goToLoginPage = async () => {
    const currentTab = await promiseTabQuery({ highlighted: true });
    await Promise.all([
      chrome.tabs.create({
        url: 'https://mochidemy.com/extension-page-login/login',
        openerTabId: currentTab[0].id,
        windowId: currentTab[0].windowId,
        index: currentTab[0].index + 1,
      }),
      trackingAction({ action: 'open_sign_up' }),
    ]);
  };

  const goToProfilePage = async () => {
    const currentTab = await promiseTabQuery({ highlighted: true });
    await chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/',
      openerTabId: currentTab[0].id,
      windowId: currentTab[0].windowId,
      index: currentTab[0].index + 1,
    });
  };

  const handlePress = (e) => {
    e.stopPropagation();
    if (e.target.value.length == 0) return;
    if (e.keyCode == 13) {
      onSearch();
    }
  };

  const setSentence = (key) => {
    if (key) {
      const result = _.replace(
        key,
        new RegExp('<br />', 'g'),
        '<br />&#8722; '
      );
      return (
        '&#8722; ' +
        (_.endsWith(result, '&#8722; ')
          ? result.substring(0, result.length - 9)
          : result)
      );
    }
    return '';
  };

  const onSavingWord = async (e, word_id) => {
    e.preventDefault();
    const identityStore = await promiseGetStorage(['identity']);
    if (!identityStore.identity) {
      $('#invite-modal').css({ display: 'block' });
    } else {
      await saveWord({ word_id }, statistics, profile)(dispatch);
      await onSearch();
    }
  };

  useEffect(async () => {
    document.addEventListener('click', closeAllSelect);
    const identityStore = await promiseGetStorage(['identity']);
    setIdentity(identityStore.identity);
    if (identityStore.identity && identityStore.identity.user_token) {
      await Promise.all([
        getProfile({ user_token: identityStore.identity.user_token })(dispatch),
        getStatistics({
          user_token: identityStore.identity.user_token,
          lang: 'vi',
        })(dispatch),
      ]);
    }
    initilizeModal();
    const mode = await promiseGetStorage('modeDictionary');
    if (mode.modeDictionary) {
      setMode(mode.modeDictionary);
    }
  }, []);

  const closeAllSelect = (elmnt) => {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x,
      y,
      i,
      xl,
      yl,
      arrNo = [];
    x = document.getElementsByClassName('select-items');
    y = document.getElementsByClassName('select-selected');
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i);
      } else {
        y[i].classList.remove('select-arrow-active');
      }
    }
    for (i = 0; i < xl; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add('select-hide');
      }
    }
  };

  const result_search = useSelector((state) => state.dictionary.result_search);
  const profile = useSelector((state) => state.account.profile);
  const statistics = useSelector((state) => state.account.statistics);

  const getAccountType = (expiredDate) => {
    if (!expiredDate) {
      return 'Free account';
    }
    const now = new Date();
    const expiredDay = new Date(expiredDate);
    if (expiredDay < now) {
      return 'Expired account';
    } else {
      return 'Premium account';
    }
  };

  return (
    <div className="mochi-popup">
      <div className="form">
        <div className="mochi__dictionary__searchbox">
          <div id="inputWrapper">
            <input
              onKeyDown={handlePress}
              value={key}
              type="text"
              placeholder="Gõ vào đây từ bạn muốn tra cứu"
              autoComplete="off"
              onChange={handleChange}
            />
            <img
              onClick={onSearch}
              src={search_icon}
              alt="search"
              width="25"
              height="25"
            />
          </div>
        </div>
      </div>
      {result_search && !_.isEmpty(result_search) && (
        <div className="dictionary-header">
          <div className="custom-select">
            <div className="select-selected" onClick={handleClickSelect}>
              {MODES[mode]}
            </div>
            <div className="select-items select-hide">
              {mode != 'us' && (
                <div onClick={() => setMode('us')}>Anh - Anh</div>
              )}
              {mode != 'vi' && (
                <div onClick={() => setMode('vi')}>Anh - Việt</div>
              )}
            </div>
          </div>
          <div
            className="dictionary-account"
            style={{
              color: '#ffcb08',
              margin: 'auto 0',
              display: 'flex',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            }}
          >
            <span
              style={{ cursor: 'pointer', margin: 'auto' }}
              onClick={identity ? goToProfilePage : goToLoginPage}
            >
              {profile && profile.display_name
                ? profile.display_name
                : 'Đăng nhập'}
            </span>
            <div
              className="mochi_avatar"
              onClick={!_.isEmpty(identity) ? goToProfilePage : goToLoginPage}
              style={{
                backgroundImage: `url(${
                  profile && profile.avatar
                    ? profile.avatar
                    : `chrome-extension://${chrome.runtime.id}${mochi_default_icon}`
                })`,
                borderColor:
                  getAccountType(identity.expired_day) == 'Premium account'
                    ? '#ffd534'
                    : '#44c016',
              }}
            ></div>
          </div>
        </div>
      )}
      {result_search &&
        !_.isEmpty(result_search) &&
        result_search[mode] &&
        result_search[mode][0] && (
          <div className="dictionary-body">
            <div className="dictionary-loudspeaker">
              <div>
                <span
                  className="pronunciation-type"
                  style={{ color: '#3382ed' }}
                >
                  BrE
                </span>
                {result_search[mode][0].audio_uk && (
                  <img
                    className="audio-icon"
                    src={iconBlue}
                    alt="bre-audio"
                    width="25"
                    height="25"
                    onClick={() => playSound(result_search[mode][0].audio_uk)}
                  />
                )}
                {result_search[mode][0].phonetic_uk}
              </div>
              <div>
                <span
                  className="pronunciation-type"
                  style={{ color: '#eb5a5a' }}
                >
                  NAmE
                </span>
                {result_search[mode][0].audio_us && (
                  <img
                    className="audio-icon"
                    src={iconRed}
                    alt="name-audio"
                    width="25"
                    height="25"
                    onClick={() =>
                      playSound(result_search[mode][0].audio_us, 'red')
                    }
                  />
                )}
                {result_search[mode][0].phonetic_us}
              </div>
            </div>
            {result_search &&
              !_.isEmpty(result_search) &&
              result_search[mode] &&
              result_search[mode][0] &&
              (view == VIEWS.collapse ? (
                <div className="single-result">
                  <div className="script">
                    <div className="main-script">
                      <span>
                        {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0
                          ? `${result_search[mode][0].detail[0].trans} ${
                              result_search[mode][0].detail[0].position
                                ? `(${result_search[mode][0].detail[0].position})`
                                : ''
                            }`
                          : `${result_search[mode][0].trans} ${
                              result_search[mode][0].position
                                ? `(${result_search[mode][0].position})`
                                : ''
                            }`}
                      </span>
                      {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0 &&
                        result_search[mode][0].detail[0].review == 1 && (
                          <a
                            href="#"
                            className="btn btn-saving-word"
                            onClick={(e) =>
                              onSavingWord(
                                e,
                                result_search[mode][0].detail[0].id ?? 0
                              )
                            }
                          >
                            <span>+</span>Lưu từ
                          </a>
                        )}
                      {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0 &&
                        result_search[mode][0].detail[0].review == 2 && (
                          <a href="#" className="btn btn-saving-word saved">
                            <img src={check_save_icon} width="10" />
                            Đã lưu
                          </a>
                        )}
                    </div>
                    <div
                      className="sub-script"
                      style={{ fontWeight: '400' }}
                      dangerouslySetInnerHTML={{
                        __html:
                          result_search[mode][0].detail &&
                          result_search[mode][0].detail.length > 0
                            ? setSentence(
                                result_search[mode][0].detail[0].en_sentence
                              )
                            : setSentence(result_search[mode][0].en_sentence),
                      }}
                    />
                  </div>
                </div>
              ) : (
                result_search[mode].map((item, index) => (
                  <div key={index}>
                    {index > 0 &&
                      (!_.isEqual(
                        item.audio_us,
                        result_search[mode][index - 1].audio_us
                      ) ||
                        !_.isEqual(
                          item.audio_uk,
                          result_search[mode][index - 1].audio_uk
                        )) && (
                        <div className="dictionary-loudspeaker">
                          <div>
                            <span
                              className="pronunciation-type"
                              style={{ color: '#3382ed' }}
                            >
                              BrE
                            </span>
                            <img
                              className="audio-icon"
                              src={audio_icon_1}
                              alt="bre-audio"
                              width="25"
                              height="25"
                              onClick={() => playSound(item.audio_uk)}
                            />
                            {item.phonetic_uk}
                          </div>
                          <div>
                            <span
                              className="pronunciation-type"
                              style={{ color: '#eb5a5a' }}
                            >
                              NAmE
                            </span>
                            <img
                              className="audio-icon"
                              src={audio_icon_2}
                              alt="name-audio"
                              width="25"
                              height="25"
                              onClick={() => playSound(item.audio_us)}
                            />
                            {item.phonetic_us}
                          </div>
                        </div>
                      )}
                    <div className="single-result">
                      <div className="script">
                        <div className="main-script">
                          <span>
                            {item.detail && item.detail.length > 0
                              ? `${item.detail[0].trans} ${
                                  item.detail[0].position
                                    ? `(${item.detail[0].position})`
                                    : ''
                                }`
                              : `${item.trans} ${
                                  item.position ? `(${item.position})` : ''
                                }`}
                          </span>
                          {item.detail &&
                            item.detail.length > 0 &&
                            item.detail[0].review == 1 && (
                              <a
                                href="#"
                                className="btn btn-saving-word"
                                onClick={(e) =>
                                  onSavingWord(
                                    e,
                                    item.detail && item.detail.length > 0
                                      ? item.detail[0].id ?? 0
                                      : item.id ?? 0
                                  )
                                }
                              >
                                <span>+</span>Lưu từ
                              </a>
                            )}
                          {item.detail &&
                            item.detail.length > 0 &&
                            item.detail[0].review == 2 && (
                              <a href="#" className="btn btn-saving-word saved">
                                <img src={check_save_icon} width="10" />
                                Đã lưu
                              </a>
                            )}
                        </div>
                        <div
                          className="sub-script"
                          style={{ fontWeight: '400' }}
                          dangerouslySetInnerHTML={{
                            __html:
                              item.detail && item.detail.length > 0
                                ? setSentence(item.detail[0].en_sentence)
                                : setSentence(item.en_sentence),
                          }}
                        />
                      </div>
                    </div>
                  </div>
                ))
              ))}
          </div>
        )}
      {result_search &&
        !_.isEmpty(result_search) &&
        result_search[mode].length <= 0 &&
        result_search.suggests.length > 0 && (
          <div className="dictionary-body">
            <div className="suggest-title">
              {mode === 'vi' ? 'Có phải bạn đang tìm từ này?' : 'Did you mean?'}
            </div>
            <div className="suggest-list">
              {result_search.suggests.map((item, index) => (
                <div className="suggest" key={index}>
                  <a href="#" onClick={(e) => onSearchSuggestion(e, item)}>
                    {item}
                  </a>
                </div>
              ))}
            </div>
            <img
              src={mochi_detective_icon}
              alt="mochi_detective_icon"
              className="mochi_detective_icon"
              width="70"
            />
          </div>
        )}
      <div
        className="dictionary-footer"
        style={{
          paddingTop:
            result_search &&
            !_.isEmpty(result_search) &&
            result_search[mode].length <= 0 &&
            result_search.suggests.length > 0
              ? '0'
              : '10px',
        }}
      >
        <div>
          {result_search &&
            !_.isEmpty(result_search) &&
            result_search[mode] &&
            result_search[mode].length > 1 &&
            (mode === 'vi' ? (
              <a href="#" onClick={changeView}>
                {view == VIEWS.collapse ? 'Xem các nghĩa khác' : 'Rút gọn'}
              </a>
            ) : (
              <a href="#" onClick={changeView}>
                {view == VIEWS.collapse ? 'See more' : 'See less'}
              </a>
            ))}
        </div>
        <div>
          <img src={dictionary_logo} alt="dictionary_logo" height="10" />
        </div>
      </div>
      <div className="mochi-modals">
        <InviteLogin />
        <SavingWordSucced />
        <SavingMoreThanOneHundredLevel1 />
        <SavingMoreThanOneHundredWords />
        <SavingSucceedWithPaidUser />
        <SavingSucceedWithoutNonUsedWebsite />
        <SavingWordSucceedWithPaidUserOnTrainingTime />
        <SearchError mode={mode} />
      </div>
    </div>
  );
};

export default Popup;
