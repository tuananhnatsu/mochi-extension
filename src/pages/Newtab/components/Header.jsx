import React, { useState, useEffect } from 'react';
import clock_icon from '../../../assets/img/clock.png';
import setting_icon from '../../../assets/img/setting.png';
import search_icon from '../../../assets/img/search.png';
import ava_icon from '../../../assets/img/mochi-default-icon.png';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import mochi_default_icon from '../../../assets/img/mochi-default-icon.png';
import book_icon from '../../../assets/img/book-icon.png';
import hourglass from '../../../assets/img/hourglass.png';
import notice from '../../../assets/img/notice.png';
import Dictionary from '../../../components/common/Dictionary';
import { search } from '../../../invoker/dictionary.invoker';
import { trackingAction } from '../../../invoker/google.invoker';
import { promiseTabQuery, promiseGetStorage } from '../../../helpers/chrome';

const OPTIONS_DATE = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
};

const styleBtnSetting = {
  backgroundImage: `url(${setting_icon})`,
  backgroundSize: 'cover',
  width: '35px',
  height: '35px',
  display: 'inline-block',
  verticalAlign: 'middle',
  position: 'relative',
  cursor: 'pointer',
};

const styleBtnAccount = {
  backgroundSize: 'cover',
  width: '31px',
  height: '31px',
  display: 'inline-block',
  verticalAlign: 'middle',
  position: 'relative',
  cursor: 'pointer',
  marginLeft: '1.5rem',
  border: '2px solid #ff9601',
  borderRadius: '50%',
};

const Header = () => {
  const [key, setKey] = useState('');

  const goToLoginPage = () => {
    const loginModal = document.getElementById('invite-modal');
    loginModal.style.display = 'block';
  };

  const goToProfilePage = async () => {
    const currentTab = await promiseTabQuery({ highlighted: true });
    await chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/',
      openerTabId: currentTab[0].id,
      windowId: currentTab[0].windowId,
      index: currentTab[0].index + 1,
    });
  };

  const [time, setTime] = useState(new Date().toTimeString().substr(0, 5));
  const [date, setDate] = useState(
    new Date().toLocaleDateString('en-US', OPTIONS_DATE)
  );
  const [now, setNow] = useState(new Date());
  const statistics = useSelector((state) => state.account.statistics);

  useEffect(() => {
    document.addEventListener('click', (e) => {
      const paths = e.composedPath();
      const isMochiDictionary = _.some(
        paths,
        (e) => e.id === 'mochi_dictionary_box'
      );
      if (!isMochiDictionary) {
        const mochi_dictionary_box = document.getElementById(
          'mochi_dictionary_box'
        );
        mochi_dictionary_box.style.display = 'none';
      }
    });
    document
      .querySelector('.close-dictionary')
      .addEventListener('click', () => {
        const mochi_dictionary_box = document.getElementById(
          'mochi_dictionary_box'
        );
        mochi_dictionary_box.style.display = 'none';
      });
    setInterval(() => {
      setTime(new Date().toTimeString().substr(0, 5));
      setDate(new Date().toLocaleDateString('en-US', OPTIONS_DATE));
      setNow(new Date());
    }, 1000);
  }, []);

  const getAccountType = (expiredDate) => {
    if (!expiredDate) {
      return 'Free account';
    }
    const now = new Date();
    const expiredDay = new Date(expiredDate);
    if (expiredDay < now) {
      return 'Expired account';
    } else {
      return 'Premium account';
    }
  };

  const profile = useSelector((state) => state.account.profile);

  const getTimeToTrain = (date) => {
    if (!date) return '';
    const targetDate = new Date(date);

    let timeDiff = targetDate - now;
    timeDiff /= 1000;
    const hour = Math.floor(timeDiff / 3600);
    const minutes = Math.floor((timeDiff - hour * 3600) / 60);
    const seconds = Math.floor(timeDiff - hour * 3600 - minutes * 60);

    return `${hour < 10 ? '0' + hour : hour}:${
      minutes < 10 ? '0' + minutes : minutes
    }:${seconds < 10 ? '0' + seconds : seconds}`;
  };

  const dispatch = useDispatch();

  const onSearch = async (keyword) => {
    await search({ key: keyword })(dispatch);
    const mochi_dictionary_box = document.getElementById(
      'mochi_dictionary_box'
    );
    mochi_dictionary_box.style.display = 'block';
  };

  const handleEnter = async (e) => {
    if (e.target.value.length == 0) return;
    if (e.keyCode == 13) {
      await onSearch(key);
    }
  };

  const handleChange = (e) => {
    if (!_.isEmpty(profile)) {
      setKey(e.target.value);
    } else {
      const loginModal = document.getElementById('invite-modal');
      loginModal.style.display = 'block';
    }
  };

  return (
    <div className="mochi-header">
      <div className="mochi__clock">
        <img src={clock_icon} alt="clock" width="20" height="20" />
        <span>{time}</span>
        <div>{date}</div>
      </div>
      <div className="mochi__setting_account">
        <div className="mochi__setacc">
          <a
            href="#"
            data-modal="setting-modal"
            className="mochi_setting modal-trigger"
            style={styleBtnSetting}
          ></a>
          {_.isEmpty(profile) && (
            <a type="button" className="mochi_login" onClick={goToLoginPage}>
              Đăng nhập
            </a>
          )}
          {!_.isEmpty(profile) && (
            <a
              type="button"
              className="mochi_account"
              style={{
                ...styleBtnAccount,
                borderColor:
                  getAccountType(profile.expired_day) == 'Premium account'
                    ? '#ff9601'
                    : '#23ac38',
                backgroundImage: `url(${profile.avatar ?? ava_icon})`,
              }}
              onClick={goToProfilePage}
            >
              <div>
                <div className="mochi_account_type">
                  {getAccountType(profile.expired_day)}
                </div>
                <div className="mochi_account_name">{profile.display_name}</div>
                <div className="mochi_account_email">{profile.email}</div>
              </div>
            </a>
          )}
        </div>
        <div className="mochi__dictionary">
          <h4>MOCHI DICTIONARY</h4>
          <div className="mochi__dictionary__searchbox">
            <div id="inputWrapper">
              <input
                type="text"
                placeholder="Gõ vào đây từ bạn muốn tra cứu"
                autoComplete="off"
                value={key}
                onChange={handleChange}
                onKeyDown={handleEnter}
              />
              <img
                onClick={async () => await onSearch(key)}
                src={search_icon}
                alt="search"
                width="25"
                height="25"
                className="input-icon"
                // className={key.length > 0 ? 'active' : ''}
              />
              <div className="mochi_dictionary_box" id="mochi_dictionary_box">
                <div className="close-dictionary">&#10006;</div>
                <Dictionary page="newtab" name={key} setName={setKey} />
              </div>
            </div>
          </div>
        </div>
        <div className="mochi__underbox" style={{ position: 'relative' }}>
          <div className="mochi_learn_english">
            <div className="mochi_learn_e_header">
              <h4>MOCHI-LEARN ENGLISH</h4>
            </div>
            {statistics && statistics.total ? (
              <div className="mochi__learn_e__content">
                <div className="mochi_learn_e_defalt">
                  <h5 style={{ marginBottom: '5px', marginTop: '1rem' }}>
                    <img
                      src={book_icon}
                      alt="book"
                      width="15"
                      style={{ marginRight: '5px' }}
                    />
                    Bạn đã học được
                  </h5>
                  <h4 style={{ marginBottom: '1rem' }}>
                    {statistics.total} từ
                  </h4>

                  <div className="mochi-chart">
                    {statistics.statistic &&
                      statistics.statistic.map((item, i) => (
                        <div className="proficient" key={i}>
                          <div className="wordAmount">{item.count} từ</div>
                          <div
                            className="drawed-chart"
                            id={`col-${i}`}
                            style={{ height: item.height ?? '' }}
                          ></div>
                          <div className="level">{i + 1}</div>
                        </div>
                      ))}
                  </div>

                  <h5 style={{ marginBottom: '5px' }}>
                    Chuẩn bị ôn tập: {statistics.review_count} từ
                  </h5>
                  {statistics.review_time &&
                  new Date(statistics.review_time) <= now ? (
                    <div className="mochi__let_learn">
                      <a
                        type="button"
                        href="https://learn.mochidemy.com/"
                        onClick={async () =>
                          await trackingAction({
                            action: 'newTab_open_website',
                          })
                        }
                        target="_blank"
                        className="mochi_training"
                      >
                        Ôn tập ngay
                      </a>
                    </div>
                  ) : (
                    <div className="mochi__let_learn">
                      <div
                        className="time_to_train"
                        style={{ cursor: 'pointer' }}
                        onClick={async () => {
                          chrome.tabs.create({
                            url: 'https://learn.mochidemy.com/',
                          });
                          await trackingAction({
                            action: 'newTab_open_website',
                          });
                        }}
                      >
                        <img src={hourglass} alt="hourglass" width="20" />
                        <span>{getTimeToTrain(statistics.review_time)}</span>
                        <img
                          className="notice"
                          src={notice}
                          alt="notice"
                          width="16"
                        ></img>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            ) : (
              <div className="mochi__learn_e__content">
                <div className="mochi_learn_e_defalt">
                  <img src={mochi_default_icon} alt="search" width="125" />
                  <div className="mochi_learn_e_defalt_intro">
                    Ghi nhớ 1000 từ vựng
                    <br />
                    trong 1 tháng với MochiMochi
                  </div>
                  <div className="mochi__let_learn_e">
                    <a
                      type="button"
                      href="https://learn.mochidemy.com/"
                      onClick={async () =>
                        await trackingAction({
                          action: 'newTab_open_website',
                        })
                      }
                      target="_blank"
                      className="mochi_login"
                    >
                      Học ngay thôi
                    </a>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
