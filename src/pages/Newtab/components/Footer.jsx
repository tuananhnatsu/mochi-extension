import React, { useEffect, useState } from 'react';
import chrome_icon from '../../../assets/img/chrome.png';
import { useSelector } from 'react-redux';

const styleBtnChrome = {
  backgroundImage: `url(${chrome_icon})`,
  backgroundSize: 'cover',
  width: '25px',
  height: '25px',
  display: 'inline-block',
  verticalAlign: 'middle',
  position: 'relative',
  cursor: 'pointer',
};

const Footer = () => {
  const [index, setIndex] = useState(0);
  const quotes = useSelector((state) =>
    state.common.quotes.filter((f) => f.active)
  );
  useEffect(() => {
    setInterval(() => {
      setIndex(index < quotes.length ? index + 1 : 0);
    }, 60 * 1000 * 60);
  }, []);
  const returnToChromeNewTab = (e) => {
    e.preventDefault();
    chrome.tabs.update({ url: 'chrome-search://local-ntp/local-ntp.html' });
  };

  return (
    <div className="mochi-footer">
      <div className="mochi__chrome">
        <a
          href="#"
          onClick={returnToChromeNewTab}
          className="mochi_chrome"
          style={styleBtnChrome}
        ></a>
      </div>
      <div className="mochi__philosophy">
        <div className="mochi__philosophy_text">
          {quotes && quotes[index] && `"${quotes[index].name}"`}
        </div>
      </div>
      <div className="mochi__align-right"></div>
    </div>
  );
};

export default Footer;
