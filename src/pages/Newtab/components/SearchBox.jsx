import React, { useEffect } from 'react';
import logo from '../../../assets/img/logo.png';
import search_icon from '../../../assets/img/search.png';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { getGgSuggestion } from '../../../invoker/google.invoker';
import { useSelector } from 'react-redux';
import { setGgSuggestion } from '../../../actions';

const SearchBox = () => {
  const suggestion = useSelector((state) => state.google.suggestions);
  const [keyword, setKeyword] = useState('');
  const [activeSug, setActiveSug] = useState(-1);
  const dispatch = useDispatch();
  const handleChange = async (e) => {
    setKeyword(e.target.value);
    setActiveSug(-1);
    if (e.target.value) {
      await getGgSuggestion(e.target.value)(dispatch);
    } else {
      resetSuggestion();
    }
  };
  const resetSuggestion = () => {
    setTimeout(() => {
      dispatch(setGgSuggestion([]));
    }, 200);
  };
  const handlePress = (e) => {
    if (e.target.value.length == 0) return;
    if (e.keyCode == 13) {
      onSearch(keyword);
    }
    if (e.keyCode == 40) {
      const active = activeSug < suggestion.length - 1 ? activeSug + 1 : 0;
      setActiveSug(active);
      setKeyword(suggestion[active].suggestion['@data']);
    }
    if (e.keyCode == 38) {
      let active = 0;
      if (activeSug <= 0) {
        active = suggestion.length - 1;
      } else active = activeSug - 1;
      setActiveSug(active);
      setKeyword(suggestion[active].suggestion['@data']);
    }
  };
  const onSearch = (keyword) => {
    if (keyword)
      chrome.tabs.update({
        url: `https://www.google.com/search?q=${keyword}&sourceid=chrome&ie=UTF-8`,
      });
  };

  return (
    <div className="mochi-searchbox">
      <div className="mochi_logo">
        <img src={logo} alt="logo" height="40" />
      </div>
      <div className="mochi_realbox">
        <div id="inputWrapper">
          <input
            className={suggestion.length > 0 ? 'showing-suggestion' : ''}
            value={keyword}
            onKeyDown={handlePress}
            onChange={handleChange}
            onBlur={resetSuggestion}
            type="text"
            placeholder="Nhập nội dung bạn muốn tìm kiếm"
            autoComplete="off"
          />
          <img
            onClick={() => onSearch(keyword)}
            src={search_icon}
            alt="search"
            width="32"
            height="32"
          />
        </div>
        <div className="gg-suggestion" hidden={suggestion.length <= 0}>
          {suggestion.map((item, i) => (
            <div
              key={i}
              className={
                'gg-suggestion-item ' + (activeSug == i ? 'activeSug' : '')
              }
              onClick={() => onSearch(item.suggestion['@data'])}
            >
              {item.suggestion['@data']}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default SearchBox;
