import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from '../../store';
import Newtab from './Newtab';
import './index.css';
import { BACKGROUNDS, QUOTES } from '../../constants/common';
import { getProfile, getStatistics } from '../../invoker/account.invoker';
import {
  setProfile,
  setStatistics,
  setQuotes,
  setBgImage,
} from '../../actions/index';
import { promiseGetStorage } from '../../helpers/chrome';

const store = configureStore();
chrome.storage.local.get('quotes', ({ quotes }) => {
  if (quotes) {
    const _quotes = QUOTES.map((m, index) => ({
      name: m,
      active: quotes.includes(index),
    }));
    store.dispatch(setQuotes(_quotes));
  }
});
chrome.storage.local.get(['disableNewtab'], async ({ disableNewtab }) => {
  // await chrome.storage.local.remove('identity');
  if (disableNewtab) {
    await chrome.tabs.update({
      url: 'chrome-search://local-ntp/local-ntp.html',
    });
  } else {
    const bgIdStore = await promiseGetStorage(['bgId']);
    const background = {
      backgroundImage: `url(${BACKGROUNDS[Number(bgIdStore.bgId ?? 0)]})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    };
    store.dispatch(setBgImage(background));
    render(
      <Provider store={store}>
        <Newtab />
      </Provider>,
      window.document.querySelector('#mochi-container')
    );
  }
});

chrome.storage.onChanged.addListener(async function (changes, namespace) {
  for (let [key, { newValue }] of Object.entries(changes)) {
    if (key === 'identity') {
      if (newValue && newValue.user_token) {
        await Promise.all([
          getProfile({ user_token: newValue.user_token })(store.dispatch),
          getStatistics({ user_token: newValue.user_token, lang: 'vi' })(
            store.dispatch
          ),
        ]);
      } else {
        store.dispatch(setProfile({}));
        store.dispatch(setStatistics({}));
      }
    }
  }
});

if (module.hot) module.hot.accept();
