import React, { useEffect } from 'react';
import { initilizeModal } from '../Content/modules/modal';
import './Newtab.scss';
import SettingModal from '../../components/modals/SettingModal';
import InviteLogin from '../../components/modals/InviteLogin';
import Thanks from '../../components/modals/Thanks';
import Header from './components/Header';
import Footer from './components/Footer';
import SearchBox from './components/SearchBox';
import { BACKGROUNDS } from '../../constants/common';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { getProfile, getStatistics } from '../../invoker/account.invoker';
import SavingWordSucced from '../../components/modals/SavingWordSucced';
import SavingMoreThanOneHundredLevel1 from '../../components/modals/SavingMoreThanOneHundredLevel1';
import SavingMoreThanOneHundredWords from '../../components/modals/SavingMoreThanOneHundredWords';
import SavingSucceedWithPaidUser from '../../components/modals/SavingSucceedWithPaidUser';
import SavingWordSucceedWithPaidUserOnTrainingTime from '../../components/modals/SavingWordSucceedWithPaidUserOnTrainingTime';
import SavingSucceedWithoutNonUsedWebsite from '../../components/modals/SavingSucceedWithoutNonUsedWebsite';
import SearchError from '../../components/modals/SearchError';
import { promiseGetStorage } from '../../helpers/chrome';

const Newtab = () => {
  const backgrounds = BACKGROUNDS;
  const dispatch = useDispatch();

  useEffect(async () => {
    initilizeModal();
    const identityStore = await promiseGetStorage(['identity']);
    const user_token = _.get(identityStore, 'identity.user_token', '');
    if (user_token) {
      await Promise.all([
        getProfile({ user_token })(dispatch),
        getStatistics({ user_token, lang: 'vi' })(dispatch),
      ]);
    }
  }, []);

  const mochi_wrapper_style = useSelector(
    (state) => state.common.background,
    shallowEqual
  );

  return (
    <div className="mochi-wrapper" style={mochi_wrapper_style}>
      <div id="content">
        <div style={{ width: '100%' }}>
          <Header />
          <SearchBox />
        </div>
        <Footer />
      </div>
      <div className="mochi-modal">
        <SettingModal />
        <InviteLogin />
        <Thanks />
        <SavingMoreThanOneHundredLevel1 />
        <SavingMoreThanOneHundredWords />
        <SavingSucceedWithPaidUser />
        <SavingSucceedWithoutNonUsedWebsite />
        <SavingWordSucceedWithPaidUserOnTrainingTime />
        <SavingWordSucced />
        <SearchError mode={'vi'} />
      </div>

      {/* Pre load to cache image */}
      <div className="pre-load" hidden>
        {backgrounds.map((background, i) => {
          return (
            <div key={i} className="background_img">
              <img src={background} alt={`background` + i} />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Newtab;
