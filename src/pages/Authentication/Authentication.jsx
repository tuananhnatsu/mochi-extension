import React, { useState, useEffect } from 'react';
import './Authentication.scss';
import mochi_login_theme_icon from '../../assets/img/mochi-login-theme-icon.png';
import {
  login,
  loginWithFacebook,
  loginWithGoogle,
  loginWithApple,
} from '../../invoker/account.invoker';
import { useDispatch, useSelector } from 'react-redux';
import { setErrorLogin } from '../../actions';
import LoginSucceed from '../../components/modals/LoginSucceed';
import ForgotPassword from '../../components/modals/ForgotPassword';
import { initilizeModal } from '../Content/modules/modal';
import { promiseTabCurrent } from '../../helpers/chrome';

const Authentication = () => {
  const [account, setAccount] = useState({
    email: '',
    password: '',
  });

  const loginWithFb = async (e) => {
    e.preventDefault();
    await loginWithFacebook(openerTabId);
  };
  const loginWithGg = async (e) => {
    e.preventDefault();
    await loginWithGoogle();
  };

  const loginWithApp = async (e) => {
    e.preventDefault();
    await loginWithApple();
  };

  const [openerTabId, setOpenerTabId] = useState(0);

  useEffect(async () => {
    const cur = await promiseTabCurrent();
    setOpenerTabId(cur.openerTabId);
    initilizeModal();
  }, []);
  const handleChange = (e) => {
    setAccount({
      ...account,
      [e.target.name]: e.target.value,
    });
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
      } else {
        dispatch(setErrorLogin({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
      } else {
        dispatch(setErrorLogin({ ...error, password: '' }));
      }
    }
  };

  const error = useSelector((state) => state.account.loginError);

  const handleBlur = (e) => {
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
        dispatch(
          setErrorLogin({
            ...error,
            email: '*Email chưa chính xác, bạn kiểm tra lại nhé',
          })
        );
      } else {
        dispatch(setErrorLogin({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
        dispatch(
          setErrorLogin({
            ...error,
            password: '*Mật khẩu yêu cầu từ 6 ký tự trở lên',
          })
        );
      } else {
        dispatch(setErrorLogin({ ...error, password: '' }));
      }
    }
  };

  const dispatch = useDispatch();
  const onLogin = async (e) => {
    e.preventDefault();
    if (validateEmail(account.email) && account.password.length >= 6) {
      await login({ ...account, device_type: 3 }, openerTabId)(dispatch);
    }
  };
  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };
  const closeTab = async () => {
    const currentTab = await promiseTabCurrent();
    chrome.tabs.remove(Number(currentTab.id));
  };

  const unhidePassword = (e) => {
    e.preventDefault();
    const passwordEl = document.getElementById('password');
    if (passwordEl.getAttribute('type') === 'text') {
      passwordEl.setAttribute('type', 'password');
      e.target.innerHTML = 'Hiển thị';
    } else {
      passwordEl.setAttribute('type', 'text');
      e.target.innerHTML = 'Ẩn';
    }
  };
  return (
    <div className="container">
      <div className="mochi-login">
        <div className="mochi-login-title">
          <button className="btn btn-cancel" onClick={closeTab}>
            &#10006;
          </button>
          Đăng nhập
        </div>
        <div className="mochi-login-body">
          <h4>Đăng nhập tài khoản học MochiMochi</h4>
          <div className="btn-section">
            <a
              type="button"
              className="btn btn-login-via-gg"
              onClick={loginWithGg}
            >
              Đăng nhập với G+
            </a>
            <a
              type="button"
              className="btn btn-login-via-fb"
              onClick={loginWithFb}
            >
              Đăng nhập với Facebook
            </a>
            <a
              type="button"
              className="btn btn-login-via-apple"
              onClick={loginWithApp}
            >
              Đăng nhập với Apple
            </a>
          </div>
          <h5>HOẶC</h5>
          <form>
            <div className="form-section">
              <div className="input_section">
                <input
                  value={account.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="text"
                  name="email"
                  className={
                    error.email
                      ? 'input-mochi-mail invalid'
                      : 'input-mochi-mail'
                  }
                  placeholder="Nhập email tài khoản học"
                />
              </div>
              {error.email && <div className="error">{error.email}</div>}
              <div className="input_section">
                <input
                  value={account.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="password"
                  name="password"
                  id="password"
                  className={
                    error.password
                      ? 'input-mochi-mail invalid'
                      : 'input-mochi-mail'
                  }
                  placeholder="Nhập chính xác mật khẩu của bạn"
                />
                {account.password.length >= 6 && (
                  <a href="#" onClick={unhidePassword}>
                    Hiển thị
                  </a>
                )}
              </div>
              {error.password && <div className="error">{error.password}</div>}
            </div>
            <div className="btn-section">
              <input
                type="submit"
                onClick={onLogin}
                className={
                  validateEmail(account.email) && account.password.length >= 6
                    ? 'btn btn-login active'
                    : 'btn btn-login'
                }
                value="Đăng nhập"
              />
            </div>
          </form>
          <a
            href="#"
            className="modal-trigger forgot_password"
            data-modal="forgot-password-modal"
          >
            Quên mật khẩu
          </a>
          <div style={{ fontWeight: 600 }}>
            Chưa có tài khoản?{' '}
            <a href="https://mochidemy.com/extension-page-login/register">
              Tạo tài khoản học mới
            </a>
          </div>
          <img
            width="150"
            className="mochi-icon"
            src={mochi_login_theme_icon}
            alt="mochi-login-theme-icon"
          />
        </div>
      </div>
      <div className="mochi-modals">
        <LoginSucceed mode="login" />
        <ForgotPassword />
      </div>
    </div>
  );
};
export default Authentication;
