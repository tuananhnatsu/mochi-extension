import React, { useEffect, useState } from 'react';
import { render } from 'react-dom';
import mochi_icon from '../../assets/img/mochi-main-icon.png';
import { search } from '../../invoker/dictionary.invoker';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import configureStore from '../../store';
import { Provider } from 'react-redux';
import './content.styles.css';
import Dictionary from '../../components/common/Dictionary';
import InviteLogin from '../../components/modals/forOtherPages/InviteLoginForContent';
import { getProfile, getStatistics } from '../../invoker/account.invoker';
import { setProfile, setStatistics } from '../../actions/index';
import SavingWordSucced from '../../components/modals/forOtherPages/SavingWordSucced';
import SavingMoreThanOneHundredLevel1 from '../../components/modals/forOtherPages/SavingMoreThanOneHundredLevel1';
import SavingMoreThanOneHundredWords from '../../components/modals/forOtherPages/SavingMoreThanOneHundredWords';
import SavingSucceedWithPaidUser from '../../components/modals/forOtherPages/SavingSucceedWithPaidUser';
import SavingWordSucceedWithPaidUserOnTrainingTime from '../../components/modals/forOtherPages/SavingWordSucceedWithPaidUserOnTrainingTime';
import SavingSucceedWithoutNonUsedWebsite from '../../components/modals/forOtherPages/SavingSucceedWithoutNonUsedWebsite';
import SearchError from '../../components/modals/forOtherPages/SearchError';
import { promiseGetStorage } from '../../helpers/chrome';

const patternEn =
  /^[A-Za-z0-9.,:;'\/\"?!& ()-|]+[A-Za-z0-9.,:;'\/\"?!& ()-|]*$/;
const Main = () => {
  const [style, setStyle] = useState({});
  const [styleBox, setStyleBox] = useState({});
  const [key, setKey] = useState('');

  const dispatch = useDispatch();

  const onSearchDictionary = async (e) => {
    // setStyle({ display: 'none' });
    e.preventDefault();
    document.body.style.cursor = 'wait';
    await search({ key })(dispatch);
    document.body.style.cursor = 'auto';
    const module = document.getElementById('mochi-extension-root');
    const shadowRoot = module.shadowRoot;
    const box = shadowRoot.getElementById('mochi_dictionary_box');

    let mochi_box = {};
    mochi_box.display = 'block';
    box.style.display = 'block';

    if (e.clientX >= window.innerWidth / 2) {
      mochi_box.left = e.pageX - 300 + 'px';
    } else {
      mochi_box.left = e.pageX + 'px';
    }

    if (e.clientY >= window.innerHeight / 2) {
      mochi_box.top = e.pageY - box.offsetHeight + 'px';
    } else {
      mochi_box.top = e.pageY + 'px';
    }

    setStyleBox(mochi_box);
  };

  useEffect(async () => {
    const identityStore = await promiseGetStorage('identity');
    if (identityStore.identity && identityStore.identity.user_token) {
      await Promise.all([
        getProfile({ user_token: identityStore.identity.user_token })(
          store.dispatch
        ),
        getStatistics({
          user_token: identityStore.identity.user_token,
          lang: 'vi',
        })(store.dispatch),
      ]);
    }

    // Test word wheather should show icon search or not
    const testWord = (word) => {
      // if (_.endsWith(word, '-') || _.startsWith(word, '-')) return false;
      return patternEn.test(word);
    };

    document.addEventListener('mouseup', (e) => {
      const paths = e.composedPath();
      const isMochiDictionary = _.some(
        paths,
        (e) => e.id === 'mochi-extension-root'
      );
      if (isMochiDictionary) {
        return;
      }
      const selection = window.getSelection();
      if (_.isEmpty(selection.toString().trim())) {
        setStyle({ display: 'none' });
      } else {
        if (!testWord(selection.toString().trim())) {
          return;
        }
        setKey(selection.toString().trim());
        const position = {
          top: e.pageY + 2 + 'px',
          left: e.pageX + 2 + 'px',
        };

        setStyle({ ...position, display: 'block' });
      }
    });
    document.addEventListener('click', (e) => {
      const paths = e.composedPath();
      const isMochiDictionary = _.some(
        paths,
        (e) => e.id === 'mochi-extension-root'
      );
      if (!isMochiDictionary) {
        setStyleBox({ display: 'none' });
      }
    });
    const module = document.getElementById('mochi-extension-root');
    const shadowRoot = module.shadowRoot;
    const closeModal = shadowRoot.querySelectorAll('.close-modal');
    const sandbox = shadowRoot.querySelectorAll('.modal-sandbox');

    const invokeClosingModal = () => {
      const modals = shadowRoot.querySelectorAll('.modal');
      for (let i = modals.length - 1; i >= 0; i--) {
        if (modals[i].style.display == 'block') {
          modals[i].style.display = 'none';
          break;
        }
      }
    };
    closeModal.forEach((item) => {
      item.addEventListener('click', () => {
        invokeClosingModal();
      });
    });
    sandbox.forEach((item) => {
      item.addEventListener('click', () => {
        invokeClosingModal();
      });
    });
  }, []);

  return (
    <div className={'toggle-extension'}>
      <a
        href="#"
        onClick={onSearchDictionary}
        className="moc-chi-btn mochi-btnToggle"
        style={style}
      >
        {chrome.runtime.id && (
          <img
            src={`chrome-extension://${chrome.runtime.id}${mochi_icon}`}
            with="24"
            height="24"
          />
        )}
      </a>
      <div
        className="mochi_dictionary_box"
        id="mochi_dictionary_box"
        style={styleBox}
      >
        <Dictionary isOtherPages={true} name={key} setName={setKey} />
      </div>
      <div className="mochi_modal">
        <InviteLogin />
        <SavingWordSucced />
        <SavingMoreThanOneHundredLevel1 />
        <SavingMoreThanOneHundredWords />
        <SavingSucceedWithPaidUser />
        <SavingSucceedWithoutNonUsedWebsite />
        <SavingWordSucceedWithPaidUserOnTrainingTime />
        <SearchError mode={'vi'} />
      </div>
    </div>
  );
};

chrome.storage.onChanged.addListener(async function (changes, namespace) {
  for (let [key, { newValue }] of Object.entries(changes)) {
    if (key === 'identity') {
      if (newValue && newValue.user_token) {
        await Promise.all([
          getProfile({ user_token: newValue.user_token })(store.dispatch),
          getStatistics({ user_token: newValue.user_token, lang: 'vi' })(
            store.dispatch
          ),
        ]);
      } else {
        store.dispatch(setProfile({}));
        store.dispatch(setStatistics({}));
      }
    }
  }
});

var link1 = document.createElement('link');
link1.setAttribute('rel', 'preconnect');
link1.setAttribute('href', 'https://fonts.googleapis.com');
document.head.appendChild(link1);

var link2 = document.createElement('link');
link2.setAttribute('rel', 'preconnect');
link2.setAttribute('href', 'https://fonts.gstatic.com');
document.head.appendChild(link2);

var link = document.createElement('link');
link.setAttribute('rel', 'stylesheet');
link.setAttribute(
  'href',
  'https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap'
);
document.head.appendChild(link);

const store = configureStore();
let extensionRoot = document.getElementById('mochi-extension-root');
if (extensionRoot) {
  // Create the shadow root
  const shadowRoot = extensionRoot.shadowRoot;

  if (shadowRoot) {
    let div = shadowRoot.getElementById('extension');
    if (!div) {
      // Create a div element
      div = document.createElement('div');
      div.setAttribute('id', 'extension');

      // Append div to shadow DOM
      shadowRoot.appendChild(div);
      render(
        <Provider store={store}>
          <Main />
        </Provider>,
        div
      );
    }
  }
}
