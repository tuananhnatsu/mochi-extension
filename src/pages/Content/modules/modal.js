import $ from "jquery";
import { forceVisible } from 'react-lazyload';

export const initilizeModal = () => {
    let count = 0
    $(".modal-trigger").click(function (e) {
        e.preventDefault();
        const dataModal = $(this).attr("data-modal");
        $("#" + dataModal).css({ "display": "block" });
        if (dataModal == "setting-modal" && count == 0) {
            setTimeout(() => {
                forceVisible();
                count++
            }, 1000)
        }
        $("body").css({"overflow-y": "hidden"}); //Prevent double scrollbar.
    });

    $(".close-modal, .modal-sandbox").click(function () {

        const modals = document.getElementsByClassName("modal");
        for (let i = modals.length - 1; i >= 0; i--) {
            if (modals[i].style.display == "block") {
                modals[i].style.display = "none";
                break;
            }
        }
        $("body").css({"overflow-y": "auto"}); //Prevent double scrollbar.
    });
}
