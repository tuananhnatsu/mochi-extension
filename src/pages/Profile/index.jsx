import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from '../../store';
import Profile from './Profile';
import './index.css';

const store = configureStore();
render(
  <Provider store={store}>
    <Profile />
  </Provider>,
  window.document.querySelector('#mochi-container')
);

if (module.hot) module.hot.accept();
