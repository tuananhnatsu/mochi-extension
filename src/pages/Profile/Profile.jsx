import React, { useState, useEffect } from 'react';
import './Profile.scss';
import calendar_cancel from '../../assets/img/calendar_cancel.png';
import calendar_checked from '../../assets/img/calendar_checked.png';
import email from '../../assets/img/email.png';
import mochi_login_theme_icon from '../../assets/img/mochi-login-theme-icon.png';
import mochi_default_icon from '../../assets/img/mochi-default-icon.png';
import { login, logout } from '../../invoker/account.invoker';
import { useDispatch, useSelector } from 'react-redux';
import { setErrorLogin } from '../../actions';
import LoginSucceed from '../../components/modals/LoginSucceed';
import { initilizeModal } from '../Content/modules/modal';
import { getProfile } from '../../invoker/account.invoker';
import { trackingAction } from '../../invoker/google.invoker';
import { formattedDate } from '../../helpers/formatDate';
import { promiseGetStorage, promiseTabCurrent } from '../../helpers/chrome';

const MODES = {
  vi: 'Từ điển Anh - Việt',
  us: 'Từ điển Anh - Anh',
};

const styleAvatar = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  width: '100px',
  height: '100px',
  borderRadius: '50%',
  border: '3px solid',
};

const Profile = () => {
  const [account, setAccount] = useState({
    email: '',
    password: '',
  });

  const [mode, setMode] = useState('vi');
  const chageModeOfDictionary = (key) => {
    setMode(key);
    chrome.storage.local.set({ modeDictionary: key });
  };
  const [disableNewtab, setDisableNewtab] = useState(true);
  const handleChangeDisabledTab = async (e) => {
    setDisableNewtab(e.target.checked);
    await chrome.storage.local.set({ disableNewtab: !e.target.checked });
  };

  const logOut = async (e) => {
    e.preventDefault();
    await logout(openerTabId);
  };
  const dispatch = useDispatch();

  const [openerTabId, setOpenerTabId] = useState(0);

  useEffect(async () => {
    document.addEventListener('click', closeAllSelect);
    const identity = await promiseGetStorage(['identity']);
    if (identity.identity && identity.identity.user_token) {
      await getProfile({ user_token: identity.identity.user_token })(dispatch);
    }
    const cur = await promiseTabCurrent();
    setOpenerTabId(cur.openerTabId);
    initilizeModal();
    const _disableNewtab = await promiseGetStorage('disableNewtab');
    setDisableNewtab(!_disableNewtab.disableNewtab);
  }, []);
  const handleChange = (e) => {
    setAccount({
      ...account,
      [e.target.name]: e.target.value,
    });
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
      } else {
        dispatch(setErrorLogin({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
      } else {
        dispatch(setErrorLogin({ ...error, password: '' }));
      }
    }
  };

  const getAccountType = (expiredDate) => {
    if (!expiredDate) {
      return 'Free account';
    }
    const now = new Date();
    const expiredDay = new Date(expiredDate);
    if (expiredDay < now) {
      return 'Expired account';
    } else {
      return 'Premium account';
    }
  };

  const error = useSelector((state) => state.account.loginError);
  const profile = useSelector((state) => state.account.profile);

  const handleBlur = (e) => {
    if (e.target.name == 'email') {
      if (!validateEmail(e.target.value)) {
        dispatch(
          setErrorLogin({
            ...error,
            email: '*Email chưa chính xác, bạn kiểm tra lại nhé',
          })
        );
      } else {
        dispatch(setErrorLogin({ ...error, email: '' }));
      }
    }
    if (e.target.name == 'password') {
      if (e.target.value.length < 6) {
        dispatch(
          setErrorLogin({
            ...error,
            password: '*Mật khẩu yêu cầu từ 6 ký tự trở lên',
          })
        );
      } else {
        dispatch(setErrorLogin({ ...error, password: '' }));
      }
    }
  };

  const onLogin = async (e) => {
    e.preventDefault();
    if (validateEmail(account.email) && account.password.length >= 6) {
      await login({ ...account, device_type: 3 }, openerTabId)(dispatch);
    }
  };
  const validateEmail = (email) => {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };
  const closeAllSelect = (elmnt) => {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x,
      y,
      i,
      xl,
      yl,
      arrNo = [];
    x = document.getElementsByClassName('select-items');
    y = document.getElementsByClassName('select-selected');
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i);
      } else {
        y[i].classList.remove('select-arrow-active');
      }
    }
    for (i = 0; i < xl; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add('select-hide');
      }
    }
  };

  const handleClickSelect = (e) => {
    e.stopPropagation();
    closeAllSelect(e.target);
    e.target.nextSibling.classList.toggle('select-hide');
    e.target.classList.toggle('select-arrow-active');
  };
  const closeTab = async () => {
    const currentTab = await promiseTabCurrent();
    chrome.tabs.remove(Number(currentTab.id));
  };
  return (
    <div className="container">
      <div className="mochi-login">
        <div className="mochi-login-title">
          <button className="btn btn-cancel" onClick={closeTab}>
            &#10006;
          </button>
          Thông tin tài khoản
        </div>
        <div className="mochi-login-body">
          <div className="mochi-information">
            <div className="mochi-avatar">
              <div
                className="avatar"
                style={{
                  ...styleAvatar,
                  borderColor:
                    getAccountType(profile.expired_day) == 'Premium account'
                      ? '#ffd534'
                      : '#44c016',
                  backgroundImage: `url(${
                    profile.avatar ?? mochi_default_icon
                  })`,
                }}
              ></div>
              {/* <img
                src={profile.avatar ?? mochi_default_icon}
                alt="mochi_default_icon"
                width="100"
                height="100"
                style={{
                  borderColor:
                    getAccountType(profile.expired_day) == 'Premium account'
                      ? '#ffd534'
                      : '#44c016',
                }}
              /> */}
              <div
                className="mochi-account-type"
                style={{
                  backgroundColor:
                    getAccountType(profile.expired_day) == 'Premium account'
                      ? '#ffd534'
                      : '#44c016',
                }}
              >
                {getAccountType(profile.expired_day)}
              </div>
            </div>
            <div className="mochi-identity">
              <h3>{profile.display_name}</h3>
              <ul>
                <li>
                  <img src={email} alt="email" height="15" />
                  Email: {profile.email}
                </li>
                <li>
                  <img
                    src={calendar_checked}
                    alt="calendar_checked"
                    height="15"
                  />
                  Ngày kích hoạt: {formattedDate(profile.created_at)}
                </li>
                {profile.expired_day && (
                  <li>
                    <img
                      src={calendar_cancel}
                      alt="calendar_cancel"
                      height="15"
                    />
                    Ngày hết hạn: {formattedDate(profile.expired_day)}
                  </li>
                )}
              </ul>
            </div>
          </div>
          <div className="mochi-options">
            <div className="mochi-setting-dictionary">
              <span>Chọn từ điển mặc định</span>
              <div className="custom-select">
                <div className="select-selected" onClick={handleClickSelect}>
                  {MODES[mode]}
                </div>
                <div className="select-items select-hide">
                  {mode != 'us' && (
                    <div onClick={() => chageModeOfDictionary('us')}>
                      Từ điển Anh - Anh
                    </div>
                  )}
                  {mode != 'vi' && (
                    <div onClick={() => chageModeOfDictionary('vi')}>
                      Từ điển Anh - Việt
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="mochi-setting-newtab">
              <span>Chức năng Mochi New Tab</span>
              <label className="switch">
                <input
                  type="checkbox"
                  onChange={handleChangeDisabledTab}
                  checked={disableNewtab}
                />
                <span className="slider round"></span>
              </label>
            </div>
          </div>
          <h4>
            Truy cập website MochiMochi để học và ôn tập với Thời Điểm Vàng
          </h4>
          <div className="btn-section">
            <a
              href="https://learn.mochidemy.com/"
              onClick={async () =>
                await trackingAction({ action: 'extension_popup_open_website' })
              }
              target="_blank"
              type="button"
              className="btn btn-login active"
            >
              Mở website ngay
            </a>
          </div>
          <div style={{ fontWeight: 600 }}>
            <a href="#" className="logout" onClick={logOut}>
              Đăng xuất
            </a>
          </div>
          <img
            width="150"
            className="mochi-icon"
            src={mochi_login_theme_icon}
            alt="mochi-login-theme-icon"
          />
        </div>
      </div>
      <div className="mochi-modals">
        <LoginSucceed />
      </div>
    </div>
  );
};
export default Profile;
