export const BASE_URL = 'https://mochien3.1-api.mochidemy.com/';

export const PRIVATE_KEY = 'M0ch1M0ch1_En_$ecret_k3y';

import default_background from '../assets/img/default-background.png';
import background_1 from '../assets/img/background-1.png';
import background_2 from '../assets/img/background-2.png';
import background_3 from '../assets/img/background-3.png';
import background_4 from '../assets/img/background-4.png';
import background_5 from '../assets/img/background-5.png';

export const BACKGROUNDS = [
  default_background,
  background_1,
  background_2,
  background_3,
  background_4,
  background_5,
];

export const ERROR_CODE = {
  EMAIL_NOT_EXIST: 'messages.email_not_exits',
  PASSWORD_INCORRECT: 'messages.password_fails',
};

export const QUOTES = [
  'Practice does not make perfect. Only perfect practice makes perfect',
  "Don't stop when you tired, stop when you're done",
  'The best way to predict the future is to create it',
  "Don't dream of winning. Train for it!",
];
