import React from 'react';
import mochi_guide_icon from '../../assets/img/mochi-guide-icon.png';
import './modal.css';
import { promiseTabQuery } from '../../helpers/chrome';
import { trackingAction } from '../../invoker/google.invoker';

const InviteLogin = () => {
  const goToLoginPage = async () => {
    const currentTab = await promiseTabQuery({ highlighted: true });
    await Promise.all([
      chrome.tabs.create({
        url: 'https://mochidemy.com/extension-page-login/login',
        openerTabId: currentTab[0].id,
        windowId: currentTab[0].windowId,
        index: currentTab[0].index + 1,
      }),
      trackingAction({ action: 'open_sign_up' }),
    ]);
  };

  const goToRegisterPage = async () => {
    const currentTab = await promiseTabQuery({ highlighted: true });
    await chrome.tabs.create({
      url: 'https://mochidemy.com/extension-page-login/register',
      openerTabId: currentTab[0].id,
      windowId: currentTab[0].windowId,
      index: currentTab[0].index + 1,
    });
  };

  return (
    <div className="modal" id="invite-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_guide_icon}
            alt="mochi_guide_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Đăng nhập tài khoản học Mochi
            <br />
            để mở khóa tất cả các tính năng
          </h4>
          <a
            type="button"
            className="mochi_modal_login close-modal not-a-close-modal"
            onClick={goToLoginPage}
          >
            Đăng nhập
          </a>
          <a
            type="button"
            className="mochi_modal_create_account close-modal not-a-close-modal"
            onClick={goToRegisterPage}
          >
            Tạo tài khoản mới
          </a>
        </div>
      </div>
    </div>
  );
};

export default InviteLogin;
