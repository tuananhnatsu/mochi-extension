import React from 'react';
import mochi_noti_icon from '../../assets/img/mochi-noti-icon.png';
import './modal.css';

const SearchError = (props) => {
  return (
    <div className="modal" id="search-error-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_noti_icon}
            alt="mochi_noti_icon"
          />
        </div>
        <div className="modal-body__invite">
          {props.mode == 'vi' ? (
            <h4>
              Mochi không tìm thấy kết quả cho từ này.
              <br />
              Bạn thử tra cứ từ khác xem nhé ^^
            </h4>
          ) : (
            <h4>
              There is no result for this word.
              <br />
              Try another word!
            </h4>
          )}
        </div>
      </div>
    </div>
  );
};

export default SearchError;
