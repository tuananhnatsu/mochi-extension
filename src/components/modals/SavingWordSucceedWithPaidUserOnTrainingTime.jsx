import React from 'react';
import { useSelector } from 'react-redux';
import mochi_noti_icon from '../../assets/img/mochi-noti-icon.png';
import { trackingAction } from '../../invoker/google.invoker';
import './modal.css';

const SavingWordSucceedWithPaidUserOnTrainingTime = () => {
  const statistics = useSelector((state) => state.account.statistics);
  return (
    <div className="modal" id="saving-ontime-to-train-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="190"
            src={mochi_noti_icon}
            alt="mochi_noti_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn đã lưu từ thành công.
            <br />
            và đang có {statistics.review_count} từ đến giờ ôn tập.
          </h4>
          <a
            type="button"
            href="https://learn.mochidemy.com/"
            onClick={async () =>
              await trackingAction({ action: 'extension_popup_open_website' })
            }
            target="_blank"
            className="mochi_modal_login"
            style={{ textDecoration: 'none' }}
          >
            Ôn tập ngay
          </a>
        </div>
      </div>
    </div>
  );
};

export default SavingWordSucceedWithPaidUserOnTrainingTime;
