import React from 'react';
import { useSelector } from 'react-redux';
import mochi_detective_icon from '../../../assets/img/mochi-detective-icon.png';
import { trackingAction } from '../../../invoker/google.invoker';
import '../modal.css';

const SavingSucceedWithoutNonUsedWebsite = () => {
  const statistics = useSelector((state) => state.account.statistics);
  return (
    <div className="modal" id="saving-non-used-website-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal detective-icon"
            width="130"
            src={`chrome-extension://${chrome.runtime.id}${mochi_detective_icon}`}
            alt="mochi_detective_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn đã lưu từ thành công.
            <br />
            Đăng nhập vào website và mở tab SỔ TAY
            <br />
            để xem từ vựng vừa lưu
          </h4>
          <a
            type="button"
            href="https://learn.mochidemy.com/"
            onClick={async () =>
              await trackingAction({ action: 'extension_popup_open_website' })
            }
            target="_blank"
            className="mochi_modal_login"
            style={{ textDecoration: 'none' }}
          >
            Mở website ngay
          </a>
        </div>
      </div>
    </div>
  );
};

export default SavingSucceedWithoutNonUsedWebsite;
