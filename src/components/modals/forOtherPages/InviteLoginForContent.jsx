import React from 'react';
import mochi_guide_icon from '../../../assets/img/mochi-guide-icon.png';
import '../modal.css';
import { trackingAction } from '../../../invoker/google.invoker';

const InviteLogin = () => {
  const goToLoginPage = async () => {
    chrome.runtime.sendMessage(
      chrome.runtime.id,
      'Authentication',
      (callback) => {}
    );
    await trackingAction({ action: 'open_sign_up' });
  };

  const goToRegisterPage = () => {
    chrome.runtime.sendMessage(chrome.runtime.id, 'Register', (callback) => {});
  };

  return (
    <div className="modal" id="invite-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={`chrome-extension://${chrome.runtime.id}${mochi_guide_icon}`}
            alt="mochi_guide_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Đăng nhập tài khoản học Mochi
            <br />
            để mở khóa tất cả các tính năng
          </h4>
          <a
            type="button"
            className="mochi_modal_login"
            onClick={goToLoginPage}
          >
            Đăng nhập
          </a>
          <a
            type="button"
            className="mochi_modal_create_account"
            onClick={goToRegisterPage}
          >
            Tạo tài khoản mới
          </a>
        </div>
      </div>
    </div>
  );
};

export default InviteLogin;
