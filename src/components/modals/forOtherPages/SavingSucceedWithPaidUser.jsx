import React from 'react';
import { useSelector } from 'react-redux';
import mochi_register_icon from '../../../assets/img/mochi-register-icon.png';
import '../modal.css';

const SavingSucceedWithPaidUser = () => {
  const statistics = useSelector((state) => state.account.statistics);
  return (
    <div className="modal" id="saving-not-in-time-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={`chrome-extension://${chrome.runtime.id}${mochi_register_icon}`}
            alt="mochi_register_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn đã lưu từ thành công.
            <br />
            Đừng quên ôn tập từ vào Thời Điểm Vàng
            <br />
            để tăng hiệu quả ghi nhớ
          </h4>
          <a
            type="button"
            className="mochi_modal_login close-modal not-a-close-modal"
            style={{ textDecoration: 'none' }}
          >
            Mình đã hiểu
          </a>
        </div>
      </div>
    </div>
  );
};

export default SavingSucceedWithPaidUser;
