import React, { useState } from 'react';
import './modal.css';
import SettingBackground from '../common/SettingBackground';
import SettingQuotes from '../common/SettingQuotes';
import SettingFeedback from '../common/SettingFeedback';

const SettingModal = () => {
  const [tab, setTab] = useState(0);

  const handleChangeTabs = (tab) => {
    setTab(tab);
  };

  return (
    <div className="modal" id="setting-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-setting">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
        </div>
        <div className="modal-body__setting">
          <div className="setting-modal__nav">
            <nav>
              <ul>
                <li
                  className={tab == 0 ? 'active' : ''}
                  onClick={() => handleChangeTabs(0)}
                >
                  Background
                </li>
                <li
                  className={tab == 1 ? 'active' : ''}
                  onClick={() => handleChangeTabs(1)}
                >
                  Quote
                </li>
                <li
                  className={tab == 2 ? 'active' : ''}
                  onClick={() => handleChangeTabs(2)}
                >
                  Góp ý
                </li>
              </ul>
            </nav>
          </div>
          {tab == 0 && <SettingBackground />}
          {tab == 1 && <SettingQuotes />}
          {tab == 2 && <SettingFeedback />}
        </div>
      </div>
    </div>
  );
};

export default SettingModal;
