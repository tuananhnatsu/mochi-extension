import React from 'react';
import mochi_register_icon from '../../assets/img/mochi-register-icon.png';
import './modal.css';

const Thanks = () => {
  return (
    <div className="modal" id="thanks-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_register_icon}
            alt="mochi_register_icon"
          />
        </div>
        <div className="modal-body__thanks">
          <h4>
            Cảm ơn bạn iu
            <br />
            đã gửi góp ý cho Mochi nhé &lt;3
          </h4>
        </div>
      </div>
    </div>
  );
};

export default Thanks;
