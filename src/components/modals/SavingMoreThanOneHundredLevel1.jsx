import React from 'react';
import { useSelector } from 'react-redux';
import mochi_guide_icon from '../../assets/img/mochi-guide-icon.png';
import { trackingAction } from '../../invoker/google.invoker';
import './modal.css';

const SavingMoreThanOneHundredLevel1 = () => {
  const profile = useSelector((state) => state.account.profile);
  const getAccountType = (expiredDate) => {
    if (!expiredDate) {
      return 'Free account';
    }
    const now = new Date();
    const expiredDay = new Date(expiredDate);
    if (expiredDay < now) {
      return 'Expired account';
    } else {
      return 'Premium account';
    }
  };
  return (
    <div className="modal" id="saving-more-than-level-1-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_guide_icon}
            alt="mochi_guide_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn đang có hơn{' '}
            {getAccountType(profile.expired_day) == 'Premium account'
              ? 100
              : 50}{' '}
            từ vựng level 1.
            <br />
            Ôn tập ngay để lưu thêm từ vào sổ tay
          </h4>
          <a
            type="button"
            href="https://learn.mochidemy.com/"
            onClick={async () =>
              await trackingAction({ action: 'extension_popup_open_website' })
            }
            target="_blank"
            className="mochi_modal_login"
            style={{ textDecoration: 'none' }}
          >
            Ôn tập ngay
          </a>
        </div>
      </div>
    </div>
  );
};

export default SavingMoreThanOneHundredLevel1;
