import React from 'react';
import mochi_congratulation from '../../assets/img/mochi-congratulation.png';
import './modal.css';

const LoginSucceed = (props) => {
  return (
    <div className="modal" id="login-succeed-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_congratulation}
            alt="mochi_congratulation"
          />
        </div>
        <div className="modal-body__thanks">
          { props.mode == "login" ? (<h4>
            Bạn đã đăng nhập thành công
            <br />
            tài khoản học MochiMochi!!
          </h4>) : (<h4>
            Bạn đã tạo tài khoản thành công
            <br />
            và mở khóa các tính năng của extension
          </h4>)}
        </div>
      </div>
    </div>
  );
};

export default LoginSucceed;
