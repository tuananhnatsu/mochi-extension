import React from 'react';
import mochi_noti_icon from '../../assets/img/mochi-noti-icon.png';
import './modal.css';

const ForgotPassword = () => {
  return (
    <div className="modal" id="forgot-password-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_noti_icon}
            alt="mochi_noti_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn hãy inbox cho Mochi
            <br />
            để được hỗ trợ đổi mật khẩu nhé
          </h4>
          <a
            type="button"
            href="https://m.me/mochidemy"
            target="_blank"
            className="mochi_modal_login"
            style={{ textDecoration: 'none' }}
          >
            INBOX CHO MOCHI
          </a>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
