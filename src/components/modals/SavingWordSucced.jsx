import React from 'react';
import { useSelector } from 'react-redux';
import mochi_noti_icon from '../../assets/img/mochi-noti-icon.png';
import { trackingAction } from '../../invoker/google.invoker';
import './modal.css';

const SavingWordSucced = () => {
  const statistics = useSelector((state) => state.account.statistics);
  return (
    <div className="modal" id="saving-succeed-modal">
      <div className="modal-sandbox"></div>
      <div className="modal-box box-invite">
        <div className="modal-header">
          <div className="close-modal">&#10006;</div>
          <img
            className="icon-modal"
            width="200"
            src={mochi_noti_icon}
            alt="mochi_noti_icon"
          />
        </div>
        <div className="modal-body__invite">
          <h4>
            Bạn đã lưu 30 từ vào sổ tay.
            <br />
            Nâng cấp tài khoản để lưu từ không giới hạn
          </h4>
          <a
            type="button"
            href="https://mochidemy.com/extension/"
            onClick={async () =>
              await trackingAction({ action: 'upgrade_premium' })
            }
            target="_blank"
            className="mochi_modal_login"
            style={{ textDecoration: 'none' }}
          >
            Nâng cấp ngay
          </a>
        </div>
      </div>
    </div>
  );
};

export default SavingWordSucced;
