import React, { useState } from 'react';
import { feedback } from '../../invoker/account.invoker';
import { useDispatch } from 'react-redux';

const SettingFeedback = () => {
  const [feedbacks, setFeedback] = useState({
    email: '',
    type: 1,
    content: '',
  });
  const handleChange = (e) => {
    setFeedback({
      ...feedbacks,
      [e.target.name]: e.target.value,
    });
  };
  const dispatch = useDispatch();
  const onFeedback = async (e) => {
    e.preventDefault();
    if (feedbacks.email && feedbacks.content) {
      await feedback({ ...feedbacks, comment: feedbacks.content })(dispatch);
      setFeedback({
        email: '',
        type: 1,
        content: '',
      });
    }
  };
  return (
    <div className="setting-modal__content">
      <div className="setting-modal__title">Góp ý</div>
      <div className="setting-modal__subtitle_fb">
        Gửi cho Mochi những góp ý của bạn để Mochi cải thiện hơn nữa nhé.
      </div>
      <div className="setting-modal__form">
        <form>
          <input
            value={feedbacks.email}
            onChange={handleChange}
            name="email"
            className={feedbacks.email ? 'valid' : ''}
            placeholder="Email"
            type="text"
          />
          <textarea
            placeholder="Những điều Mochi có thể cải thiện hơn nữa"
            rows="5"
            name="content"
            className={feedbacks.content ? 'valid' : ''}
            value={feedbacks.content}
            onChange={handleChange}
          ></textarea>
          <input
            type="submit"
            onClick={onFeedback}
            className={
              feedbacks.email && feedbacks.content
                ? 'btn btn-login active'
                : 'btn btn-login'
            }
            value="Gửi"
          />
        </form>
      </div>
    </div>
  );
};

export default SettingFeedback;
