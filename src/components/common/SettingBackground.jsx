import React, { useEffect } from 'react';
import { BACKGROUNDS } from '../../constants/common';
import Lazyload from 'react-lazyload';
import Placeholder from './Placeholder';
import check_icon from '../../assets/img/check.png';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setBgImage } from '../../actions/common';
import _ from 'lodash';
import $ from 'jquery';
import { promiseGetStorage } from '../../helpers/chrome';

const SettingBackground = () => {
  const backgrounds = BACKGROUNDS;
  const [bgId, setBgId] = useState(0);
  const dispatch = useDispatch();

  const changeBg = async (i) => {
    const bgIdStore = await promiseGetStorage(['bgId']);
    const bgId = _.get(bgIdStore, 'bgId', 0);
    if (i != bgId) {
      const identityStore = await promiseGetStorage(['identity']);
      if (identityStore.identity) {
        setBgId(i);
        await chrome.storage.local.set({ bgId: i });
        const background = {
          backgroundImage: `url(${BACKGROUNDS[i]})`,
        };
        dispatch(setBgImage(background));
      } else {
        $('#setting-modal').css({ display: 'none' });
        $('#invite-modal').css({ display: 'block' });
      }
    }
  };

  useEffect(() => {
    chrome.storage.local.get(['bgId'], ({ bgId }) => {
      if (bgId) {
        setBgId(Number(bgId));
      }
    });
  }, []);

  return (
    <div className="setting-modal__content">
      <div className="setting-modal__title">Background</div>
      <div className="setting-modal__listbg">
        {backgrounds.map((background, i) => {
          return (
            <div key={i} className="background_img">
              <Lazyload once height={120} placeholder={<Placeholder />}>
                <img
                  onClick={() => changeBg(i)}
                  className="bg_img"
                  src={background}
                  alt={`background` + i}
                />
                {bgId == i && (
                  <img
                    className="bg_check"
                    src={check_icon}
                    height={20}
                    width={20}
                    alt="check_icon"
                  />
                )}
              </Lazyload>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SettingBackground;
