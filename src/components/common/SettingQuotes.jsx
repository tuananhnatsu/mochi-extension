import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import checked_icon from '../../assets/img/checked-icon.png';
import unchecked_icon from '../../assets/img/unchecked-icon.png';
import add from '../../assets/img/add.png';
import { setQuotes } from '../../actions';
import $ from 'jquery';
import { promiseGetStorage } from '../../helpers/chrome';
import _ from 'lodash';

const SettingQuotes = () => {
  const quotes = useSelector((state) => state.common.quotes);
  const [vquotes, setVQuote] = useState();

  const addQuotes = async () => {
    const identityStore = await promiseGetStorage('identity');
    if (identityStore.identity) {
      await chrome.tabs.create({ url: 'https://forms.gle/Z29FP584bgheH9rC7' });
    } else {
      $('#setting-modal').css({ display: 'none' });
      $('#invite-modal').css({ display: 'block' });
    }
  };

  const dispatch = useDispatch();
  const triggerQuote = (quote) => {
    let virtual_quotes = [...quotes];
    const index = _.indexOf(virtual_quotes, quote);
    if (index < 0) return;
    virtual_quotes[index].active = !virtual_quotes[index].active;
    chrome.storage.local.set({
      quotes: virtual_quotes
        .map((item, index) => (item.active ? index : -1))
        .filter((f) => f >= 0),
    });
    dispatch(setQuotes(virtual_quotes));
  };

  useEffect(() => {
    const _quotes = _.sortBy(quotes, [
      function (o) {
        return !o.active;
      },
    ]);
    setVQuote(_quotes);
  }, [quotes]);

  return (
    <div className="setting-modal__content">
      <div className="setting-modal__title">Quote</div>
      <div className="setting-modal__subtitle">Mochi Quotes</div>
      <div className="setting-modal__listquotes">
        <ul className="quote_list">
          {vquotes &&
            vquotes.map((quote, index) => (
              <li className="quote_item" key={index}>
                <img
                  onClick={() => triggerQuote(quote)}
                  src={quote.active ? checked_icon : unchecked_icon}
                  alt="checked"
                  width="20"
                  height="20"
                />
                {quote.name}
              </li>
            ))}
        </ul>
      </div>
      <div className="your_quotes_section">
        Quotes của bạn
        <div className="add_quote" onClick={addQuotes}>
          <img src={add} alt="add" width="17" height="17" />
          Thêm quote của bạn
        </div>
      </div>
    </div>
  );
};

export default SettingQuotes;
