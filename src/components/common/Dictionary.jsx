import React, { useState, useEffect, Fragment } from 'react';
import { useSelector } from 'react-redux';
import dictionary_logo from '../../assets/img/dictionary-logo.png';
import audio_icon_1 from '../../assets/img/audio-icon-1.png';
import audio_icon_blue_0 from '../../assets/img/audio-icon-blue-0.png';
import audio_icon_blue_1 from '../../assets/img/audio-icon-blue-1.png';
import audio_icon_blue_2 from '../../assets/img/audio-icon-blue-2.png';

import audio_icon_2 from '../../assets/img/audio-icon-2.png';
import audio_icon_red_0 from '../../assets/img/audio-icon-red-0.png';
import audio_icon_red_1 from '../../assets/img/audio-icon-red-1.png';
import audio_icon_red_2 from '../../assets/img/audio-icon-red-2.png';

import mochi_default_icon from '../../assets/img/mochi-default-icon.png';
import mochi_detective_icon from '../../assets/img/mochi-detective-icon.png';
import check_save_icon from '../../assets/img/check-save-icon.png';
import _ from 'lodash';
import {
  search,
  saveWord,
  saveWordForOtherPages,
} from '../../invoker/dictionary.invoker';
import { trackingAction } from '../../invoker/google.invoker';
import { useDispatch } from 'react-redux';
import { promiseGetStorage } from '../../helpers/chrome';

const VIEWS = {
  collapse: 'collapse',
  expand: 'expand',
};

const MODES = {
  vi: 'Anh - Việt',
  us: 'Anh - Anh',
};

const Dictinary = (props) => {
  const [mode, setMode] = useState('vi');
  const [view, setView] = useState(VIEWS.collapse);
  const [iconBlue, setIconBlue] = useState(audio_icon_1);
  const [iconRed, setIconRed] = useState(audio_icon_2);
  const result_search = useSelector((state) => state.dictionary.result_search);

  const playSound = async (link, type = 'blue') => {
    if (!link) return;
    const sound = new Audio(link);
    const sequentiallySetIcon = (icon) =>
      new Promise((resolve, reject) => {
        setTimeout(() => {
          type == 'blue' ? setIconBlue(icon) : setIconRed(icon);
          resolve(1);
        }, 200);
      });
    const animateSoundIcon = async () => {
      if (type == 'blue') {
        setIconBlue(audio_icon_blue_0);
        await sequentiallySetIcon(audio_icon_blue_1);
        await sequentiallySetIcon(audio_icon_blue_2);
        await sequentiallySetIcon(audio_icon_1);
      } else {
        setIconRed(audio_icon_red_0);
        await sequentiallySetIcon(audio_icon_red_1);
        await sequentiallySetIcon(audio_icon_red_2);
        await sequentiallySetIcon(audio_icon_2);
      }
    };
    await Promise.all([animateSoundIcon(), sound.play()]);
  };
  const dispatch = useDispatch();

  const onSearchSuggestion = async (e, key) => {
    e.preventDefault();
    props.setName(key);
    await search({ key })(dispatch);
  };

  const changeView = (e) => {
    e.preventDefault();
    setView(view == VIEWS.expand ? VIEWS.collapse : VIEWS.expand);
  };

  const setSentence = (key) => {
    if (key) {
      const result = _.replace(
        key,
        new RegExp('<br />', 'g'),
        '<br />&#8722; '
      );
      return (
        '&#8722; ' +
        (_.endsWith(result, '&#8722; ')
          ? result.substring(0, result.length - 9)
          : result)
      );
    }
    return '';
  };

  const goToLoginPage = async () => {
    chrome.runtime.sendMessage(
      chrome.runtime.id,
      'Authentication',
      (callback) => {
        console.log(callback);
      }
    );
    await trackingAction({ action: 'open_sign_up' });
  };

  const goToProfilePage = async () => {
    chrome.runtime.sendMessage(chrome.runtime.id, 'Profile', (callback) => {
      console.log(callback);
    });
  };
  const identity = useSelector((state) => state.account.profile);
  const statistics = useSelector((state) => state.account.statistics);
  const getAccountType = (expiredDate) => {
    if (!expiredDate) {
      return 'Free account';
    }
    const now = new Date();
    const expiredDay = new Date(expiredDate);
    if (expiredDay < now) {
      return 'Expired account';
    } else {
      return 'Premium account';
    }
  };
  useEffect(async () => {
    const mode = await promiseGetStorage('modeDictionary');
    if (mode.modeDictionary) {
      setMode(mode.modeDictionary);
    }
  }, []);

  const onSavingWord = async (e, word_id) => {
    e.preventDefault();
    if (_.isEmpty(identity)) {
      const module = document.getElementById('mochi-extension-root');
      const shadowRoot = module.shadowRoot;
      const box = shadowRoot.getElementById('invite-modal');
      box.style.display = 'block';
    } else {
      if (props.isOtherPages) {
        await saveWordForOtherPages(
          { word_id },
          statistics,
          identity
        )(dispatch);
      }
      await saveWord({ word_id }, statistics, identity)(dispatch);
      await search({ key: props.name })(dispatch);
    }
  };

  const changeMode = (e, value) => {
    e.preventDefault();
    setMode(value);
  };

  return (
    <div className="mochi_container">
      <div className="mochi_header">
        <ul>
          <li className={mode === 'vi' ? 'active' : ''}>
            <a href="#" onClick={(e) => changeMode(e, 'vi')}>
              Anh - Việt
            </a>
          </li>
          <li className={mode === 'us' ? 'active' : ''}>
            <a href="#" onClick={(e) => changeMode(e, 'us')}>
              Anh - Anh
            </a>
          </li>
        </ul>
        <div
          className="dictionary-account"
          style={{
            color: '#ffcb08',
            margin: 'auto 0',
            maxWidth: '120px',
            display: 'flex',
          }}
        >
          <span
            style={{
              cursor: 'pointer',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              margin: 'auto',
            }}
            onClick={identity ? goToProfilePage : goToLoginPage}
          >
            {props.page !== 'newtab' &&
              (identity && identity.display_name
                ? identity.display_name
                : 'Đăng nhập')}
          </span>
          {props.page !== 'newtab' && (
            <div
              className="mochi_avatar"
              onClick={!_.isEmpty(identity) ? goToProfilePage : goToLoginPage}
              style={{
                backgroundImage: `url(${
                  identity && identity.avatar
                    ? identity.avatar
                    : `chrome-extension://${chrome.runtime.id}${mochi_default_icon}`
                })`,
                borderColor:
                  getAccountType(identity.expired_day) == 'Premium account'
                    ? '#ffd534'
                    : '#44c016',
              }}
            ></div>
          )}
        </div>
      </div>
      {result_search &&
        !_.isEmpty(result_search) &&
        result_search[mode] &&
        result_search[mode][0] && (
          <div className="dictionary-body">
            <h4>{props.name}</h4>
            <div className="dictionary-loudspeaker">
              <div>
                <span
                  className="pronunciation-type"
                  style={{ color: '#3382ed' }}
                >
                  BrE
                </span>
                {result_search[mode][0].audio_uk && (
                  <img
                    className="audio-icon"
                    src={`chrome-extension://${chrome.runtime.id}${iconBlue}`}
                    alt="bre-audio"
                    width="25"
                    height="25"
                    onClick={() => playSound(result_search[mode][0].audio_uk)}
                  />
                )}
                {result_search[mode][0].phonetic_uk}
              </div>
              <div>
                <span
                  className="pronunciation-type"
                  style={{ color: '#eb5a5a' }}
                >
                  NAmE
                </span>
                {result_search[mode][0].audio_us && (
                  <img
                    className="audio-icon"
                    src={`chrome-extension://${chrome.runtime.id}${iconRed}`}
                    alt="name-audio"
                    width="25"
                    height="25"
                    onClick={() =>
                      playSound(result_search[mode][0].audio_us, 'red')
                    }
                  />
                )}
                {result_search[mode][0].phonetic_us}
              </div>
            </div>
            {result_search &&
              !_.isEmpty(result_search) &&
              result_search[mode] &&
              result_search[mode][0] &&
              (view == VIEWS.collapse ? (
                <div className="single-result">
                  <div className="script">
                    <div className="main-script">
                      <span>
                        {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0
                          ? `${result_search[mode][0].detail[0].trans} ${
                              result_search[mode][0].detail[0].position
                                ? `(${result_search[mode][0].detail[0].position})`
                                : ''
                            }`
                          : `${result_search[mode][0].trans} ${
                              result_search[mode][0].position
                                ? `(${result_search[mode][0].position})`
                                : ''
                            }`}
                      </span>
                      {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0 &&
                        result_search[mode][0].detail[0].review == 1 && (
                          <a
                            href="#"
                            className="btn btn-saving-word"
                            onClick={(e) =>
                              onSavingWord(
                                e,
                                result_search[mode][0].detail &&
                                  result_search[mode][0].detail.length > 0
                                  ? result_search[mode][0].detail[0].id ?? 0
                                  : result_search[mode][0].id ?? 0
                              )
                            }
                          >
                            <span>+</span>Lưu từ
                          </a>
                        )}
                      {result_search[mode][0].detail &&
                        result_search[mode][0].detail.length > 0 &&
                        result_search[mode][0].detail[0].review == 2 && (
                          <a href="#" className="btn btn-saving-word saved">
                            <img
                              src={`chrome-extension://${chrome.runtime.id}${check_save_icon}`}
                              width="10"
                            />
                            Đã lưu
                          </a>
                        )}
                    </div>
                    <div
                      className="sub-script"
                      style={{ fontWeight: '400' }}
                      dangerouslySetInnerHTML={{
                        __html:
                          result_search[mode][0].detail &&
                          result_search[mode][0].detail.length > 0
                            ? setSentence(
                                result_search[mode][0].detail[0].en_sentence
                              )
                            : setSentence(result_search[mode][0].en_sentence),
                      }}
                    />
                  </div>
                </div>
              ) : (
                result_search[mode].map((item, index) => (
                  <div key={index}>
                    {index > 0 &&
                      (!_.isEqual(
                        item.audio_us,
                        result_search[mode][index - 1].audio_us
                      ) ||
                        !_.isEqual(
                          item.audio_uk,
                          result_search[mode][index - 1].audio_uk
                        )) && (
                        <Fragment>
                          <h4>{props.name}</h4>

                          <div className="dictionary-loudspeaker">
                            <div>
                              <span
                                className="pronunciation-type"
                                style={{ color: '#3382ed' }}
                              >
                                BrE
                              </span>
                              <img
                                className="audio-icon"
                                src={`chrome-extension://${chrome.runtime.id}${audio_icon_1}`}
                                alt="bre-audio"
                                width="25"
                                height="25"
                                onClick={() => playSound(item.audio_uk)}
                              />
                              {item.phonetic_uk}
                            </div>
                            <div>
                              <span
                                className="pronunciation-type"
                                style={{ color: '#eb5a5a' }}
                              >
                                NAmE
                              </span>
                              <img
                                className="audio-icon"
                                src={`chrome-extension://${chrome.runtime.id}${audio_icon_2}`}
                                alt="name-audio"
                                width="25"
                                height="25"
                                onClick={() => playSound(item.audio_us)}
                              />
                              {item.phonetic_us}
                            </div>
                          </div>
                        </Fragment>
                      )}
                    <div className="single-result">
                      <div className="script">
                        <div className="main-script">
                          <span>
                            {item.detail && item.detail.length > 0
                              ? `${item.detail[0].trans} ${
                                  item.detail[0].position
                                    ? `(${item.detail[0].position})`
                                    : ''
                                }`
                              : `${item.trans} ${
                                  item.position ? `(${item.position})` : ''
                                }`}
                          </span>
                          {item.detail &&
                            item.detail.length > 0 &&
                            item.detail[0].review == 1 && (
                              <a
                                href="#"
                                className="btn btn-saving-word"
                                onClick={(e) =>
                                  onSavingWord(
                                    e,
                                    item.detail && item.detail.length > 0
                                      ? item.detail[0].id ?? 0
                                      : item.id ?? 0
                                  )
                                }
                              >
                                <span>+</span>Lưu từ
                              </a>
                            )}
                          {item.detail &&
                            item.detail.length > 0 &&
                            item.detail[0].review == 2 && (
                              <a href="#" className="btn btn-saving-word saved">
                                <img
                                  src={`chrome-extension://${chrome.runtime.id}${check_save_icon}`}
                                  width="10"
                                />
                                Đã lưu
                              </a>
                            )}
                        </div>
                        <div
                          className="sub-script"
                          style={{ fontWeight: '400' }}
                          dangerouslySetInnerHTML={{
                            __html:
                              item.detail && item.detail.length > 0
                                ? setSentence(item.detail[0].en_sentence)
                                : setSentence(item.en_sentence),
                          }}
                        />
                      </div>
                    </div>
                  </div>
                ))
              ))}
          </div>
        )}
      {result_search &&
        !_.isEmpty(result_search) &&
        result_search[mode].length <= 0 &&
        result_search.suggests.length > 0 && (
          <div className="dictionary-body">
            <div className="suggest-title">
              {mode === 'vi' ? 'Có phải bạn đang tìm từ này?' : 'Did you mean?'}
            </div>
            <div className="suggest-list">
              {result_search.suggests.map((item, index) => (
                <div className="suggest" key={index}>
                  <a href="#" onClick={(e) => onSearchSuggestion(e, item)}>
                    {item}
                  </a>
                </div>
              ))}
            </div>
            <img
              src={`chrome-extension://${chrome.runtime.id}${mochi_detective_icon}`}
              alt="mochi_detective_icon"
              className="mochi_detective_icon"
              width="70"
            />
          </div>
        )}
      <div
        className="dictionary-footer"
        style={{
          paddingTop:
            result_search &&
            !_.isEmpty(result_search) &&
            result_search[mode].length <= 0 &&
            result_search.suggests.length > 0
              ? '0'
              : '10px',
        }}
      >
        <div>
          {result_search &&
            !_.isEmpty(result_search) &&
            result_search[mode] &&
            result_search[mode].length > 1 &&
            (mode === 'vi' ? (
              <a href="#" onClick={changeView}>
                {view == VIEWS.collapse ? 'Xem các nghĩa khác' : 'Rút gọn'}
              </a>
            ) : (
              <a href="#" onClick={changeView}>
                {view == VIEWS.collapse ? 'See more' : 'See less'}
              </a>
            ))}
        </div>
        <div>
          <img
            src={`chrome-extension://${chrome.runtime.id}${dictionary_logo}`}
            alt="dictionary_logo"
            height="10"
          />
        </div>
      </div>
    </div>
  );
};

export default Dictinary;
