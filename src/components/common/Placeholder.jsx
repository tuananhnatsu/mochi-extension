import React from "react";
import './Placeholder.css'

const Placeholder = () => {
    return (
        <div>
            <div className="loader">
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
            </div>
        </div>
    )
}

export default Placeholder;