import { SET_RESULT_SEARCH } from '../actions'
// import { BACKGROUNDS } from '../constants/common'
const initialState = {
    result_search: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_RESULT_SEARCH: return {
            ...state,
            result_search: action.data
        }
        default:
            return state;
    }
};