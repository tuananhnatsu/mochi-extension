import { combineReducers } from 'redux';
import account from './account.reducer';
import google from './google.reducer';
import common from './common.reducer';
import dictionary from './dictionary.reducer';

const reducers = combineReducers({
    account,
    google,
    common,
    dictionary
});
export default reducers;