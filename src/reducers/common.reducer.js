import { SET_BACKGROUND_IMAGE, SET_QUOTES } from '../actions'
import { QUOTES } from '../constants/common'
const initialState = {
    background: {},
    quotes: QUOTES.map(m => ({
        name: m,
        active: true
    }))
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_BACKGROUND_IMAGE: return {
            ...state,
            background: action.data
        }
        case SET_QUOTES: return {
            ...state,
            quotes: action.data
        }
        default:
            return state;
    }
};