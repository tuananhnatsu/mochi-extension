import { SET_ERROR_LOGIN, SET_ERROR_REGISTER, SET_PROFILE, SET_STATISTICS } from '../actions'

const initialState = {
    loginError: {},
    registerError: {},
    profile: {},
    statistics: {},
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_STATISTICS: return {
            ...state,
            statistics: action.data
        }
        case SET_PROFILE: return {
            ...state,
            profile: action.data
        }
        case SET_ERROR_LOGIN: return {
            ...state,
            loginError: action.data
        }
        case SET_ERROR_REGISTER: return {
            ...state,
            registerError: action.data
        }
        default:
            return state;
    }
};