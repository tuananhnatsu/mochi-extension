import { SET_GG_SUGGESTION } from '../actions'

const initialState = {
    suggestions: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_GG_SUGGESTION: return {
            ...state,
            suggestions: action.data
        }

        default:
            return state;
    }
};