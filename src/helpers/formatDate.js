function pad2(n) {
  return (n < 10 ? '0' : '') + n;
}

export const formattedDate = (_date) => {
  if (!_date) return ''
  var date = new Date(_date);
  var month = pad2(date.getMonth() + 1); //months (0-11)
  var day = pad2(date.getDate()); //day (1-31)
  var year = date.getFullYear();
  return day + '/' + month + '/' + year;
};
