export const promiseTabQuery = (options) => {
  return new Promise((resolve, reject) => {
    chrome.tabs.query(options, (tabs) => {
      resolve(tabs);
    });
  });
};

export const promiseTabCurrent = () => {
  return new Promise((resolve, reject) => {
    chrome.tabs.getCurrent((tab) => {
      resolve(tab);
    });
  });
};

export const promiseGetStorage = (key) => {
  return new Promise((resolve, reject) => {
    chrome.storage.local.get(key, (res) => {
      resolve(res);
    });
  });
};
